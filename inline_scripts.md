If you have enabled inline scripts, you can use Python as follows:


PUT test/doc/1
{
  "num": 1.0
}

PUT test/doc/2
{
  "num": 2.0
}

GET test/_search
{
  "query": {
    "function_score": {
      "script_score": {
        "script": {
          "inline": "doc[\"num\"].value * factor",
          "lang": "python",
          "params": {
            "factor": 2
          }
        }
      }
    }
  }
}





If you have enabled stored scripts, you can use Python as follows:

PUT test/doc/1
{
  "num": 1.0
}

PUT test/doc/2
{
  "num": 2.0
}

POST _scripts/python/my_script  
{
  "script": "doc[\"num\"].value * factor"
}

GET test/_search
{
  "query": {
    "function_score": {
      "script_score": {
        "script": {
          "stored": "my_script", 
          "lang": "python",
          "params": {
            "factor": 2
          }
        }
      }
    }
  }
}



	
We store the script under the id my_script.

The function score query retrieves the script with id my_script.





You can save your scripts to a file in the config/scripts/ directory on every node. The .py file suffix identifies the script as containing Python:

First, save this file as config/scripts/my_script.py on every node in the cluster:

doc["num"].value * factor

PUT test/doc/1
{
  "num": 1.0
}

PUT test/doc/2
{
  "num": 2.0
}

GET test/_search
{
  "query": {
    "function_score": {
      "script_score": {
        "script": {
          "file": "my_script", 
          "lang": "python",
          "params": {
            "factor": 2
          }
        }
      }
    }
  }
}


	
The function score query retrieves the script with filename my_script.py.

















