
`[WARN ] 2022-04-01 09:16:54.714 [[main]>worker1] elasticsearch - Could not index event to Elasticsearch. {:status=>400, :action=>["index", {:_id=>nil, :_index=>"snmp02-2022.04", :routing=>nil, :_type=>"_doc"}, #<LogStash::Event:0x736016be>], :response=>{"index"=>{"_index"=>"snmp02-2022.04", "_type"=>"_doc", "_id"=>"XqBp5H8Bsda4FG00u02", "status"=>400, "error"=>{"type"=>"illegal_argument_exception", "reason"=>"Limit of mapping depth [20] in index [snmp02-2022.04] has been exceeded due to object field `

##iso.org.dod.internet.private.enterprises.1145.2.2.10.2.3.1.7.15.47.37.111.109.109.111.110.47.77.79.22.73.15.66.88.75.95.10.10


```
PUT snmp02*/_settings
{
  "index.mapping.depth.limit": 200
}
```


```
GET snmp02-2022.04/_settings
{
  "snmp02-2022.04" : {
    "settings" : {
      "index" : {
        "mapping" : {
          "depth" : {
            "limit" : "200"
          }
        },
        "number_of_shards" : "3",
        "provided_name" : "snmp02-2022.04",
        "creation_date" : "1648771211143",
        "number_of_replicas" : "1",
        "uuid" : "tsjqFnMdQ52vDXowq2Sgdaw",
        "version" : {
          "created" : "7090199"
        }
      }
    }
  }
}
```


	iso.org.dod.internet.private.enterprises.3375.2.2.11.2.3.1.7.35.47.67.111.109.109.111.110.47.77.79.66.73.76.66.78.75.95.11.23.84.73.86.69.95.32.69.66.95.72.84.84.44.95.86.83
