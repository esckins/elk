
```

systemctl stop filebeat


cd /etc/
cp -r filebeat filebeatTest

cd /var/log
mkdir filebeatTest

cd/var/lib/
cp -r filebeat filebeatTest


cd /usr/lib/systemd/system
vi filebeatTest.service

systemctl daemon-reload

cat filebeatTest.service

[Unit]
Description=Filebeat sends log files to Logstash.
Documentation=https://www.elastic.co/products/beats/filebeat
Wants=network-online.target
After=network-online.target

[Service]

Environment="BEAT_LOG_OPTS="
Environment="BEAT_CONFIG_OPTS=-c /etc/filebeatTest/filebeatTest.yml"
Environment="BEAT_PATH_OPTS=--path.home /usr/share/filebeat --path.config /etc/filebeatTest --path.data /var/lib/filebeatTest --path.logs /var/log/filebeatTest"

ExecStart=/usr/share/filebeat/bin/filebeat -c /etc/filebeatTest/filebeatTest.yml -path.home /usr/share/filebeatTest -path.config /etc/filebeatTest -path.data /var/lib/filebeatTest -path.logs /var/log/filebeatTest
Restart=always

[Install]
WantedBy=multi-user.target

systemctl start filebeat filebeatTest

ps -ax |grep filebeat

systemctl enable filebeat02
systemctl daemon-reload

systemctl restart filebeat filebeatTest
```


[configPermission](https://www.elastic.co/guide/en/beats/libbeat/current/config-file-permissions.html)

[DiscussElastic](https://discuss.elastic.co/t/how-to-install-and-configure-multiple-filebeat-in-linux-instance/130466/4)

[GoogleGroupsFilebeat](https://groups.google.com/g/wazuh/c/cWV2fvTSMCs)
