
● filebeat.service - Filebeat sends log files to Logstash or directly to Elasticsearch.
   Loaded: loaded (/usr/lib/systemd/system/filebeat.service; enabled; vendor preset: disabled)
   Active: failed (Result: start-limit) since Wed 2022-10-19 14:31:01 +03; 48s ago
     Docs: https://www.elastic.co/products/beats/filebeat
  Process: 101399 ExecStart=/usr/share/filebeat/bin/filebeat -c /etc/filebeat/filebeat.yml -path.home /usr/share/filebeat -path.config /etc/filebeat -path.data /var/lib/filebeat -path.logs /var/log/filebeat (code=exited, status=1/FAILURE)
 Main PID: 101399 (code=exited, status=1/FAILURE)

Oct 19 14:31:01  systemd[1]: Unit filebeat.service entered failed state.
Oct 19 14:31:01 host10 systemd[1]: filebeat.service failed.
Oct 19 14:31:01 host10 systemd[1]: filebeat.service holdoff time over, scheduling restart.
Oct 19 14:31:01 host10 systemd[1]: Stopped Filebeat sends log files to Logstash or directly to Elasticsearch..
Oct 19 14:31:01 host10 systemd[1]: start request repeated too quickly for filebeat.service
Oct 19 14:31:01 host10 systemd[1]: Failed to start Filebeat sends log files to Logstash or directly to Elasticsearch..
Oct 19 14:31:01 host10 systemd[1]: Unit filebeat.service entered failed state.
Oct 19 14:31:01 host10 systemd[1]: filebeat.service failed.


2022-10-19T14:31:01.052+0300    INFO    [beat]  instance/beat.go:784    Host info       {"system_info": {"host": {"architecture":"x86_64","boot_time":"2022-10-06T00:45:32+03:00","containerized":true,"hostname":"host10","ips":["127.0.0.1/8","10.230.30.67/24"],"kernel_version":"5.4.17-2011.5.3.el7uek.x86_64","mac_addresses":["00:50:56:9a:db:47"],"os":{"family":"","platform":"ol","name":"Oracle Linux Server","version":"7.9","major":7,"minor":9,"patch":0},"timezone":"+03","timezone_offset_sec":10800,"id":"f7f28ca297af483e91fbe54ba6183d11"}}}
2022-10-19T14:31:01.052+0300    INFO    [beat]  instance/beat.go:813    Process info    {"system_info": {"process": {"capabilities": {"inheritable":null,"permitted":["chown","dac_override","dac_read_search","fowner","fsetid","kill","setgid","setuid","setpcap","linux_immutable","net_bind_service","net_broadcast","net_admin","net_raw","ipc_lock","ipc_owner","sys_module","sys_rawio","sys_chroot","sys_ptrace","sys_pacct","sys_admin","sys_boot","sys_nice","sys_resource","sys_time","sys_tty_config","mknod","lease","audit_write","audit_control","setfcap","mac_override","mac_admin","syslog","wake_alarm","block_suspend","audit_read"],"effective":["chown","dac_override","dac_read_search","fowner","fsetid","kill","setgid","setuid","setpcap","linux_immutable","net_bind_service","net_broadcast","net_admin","net_raw","ipc_lock","ipc_owner","sys_module","sys_rawio","sys_chroot","sys_ptrace","sys_pacct","sys_admin","sys_boot","sys_nice","sys_resource","sys_time","sys_tty_config","mknod","lease","audit_write","audit_control","setfcap","mac_override","mac_admin","syslog","wake_alarm","block_suspend","audit_read"],"bounding":["chown","dac_override","dac_read_search","fowner","fsetid","kill","setgid","setuid","setpcap","linux_immutable","net_bind_service","net_broadcast","net_admin","net_raw","ipc_lock","ipc_owner","sys_module","sys_rawio","sys_chroot","sys_ptrace","sys_pacct","sys_admin","sys_boot","sys_nice","sys_resource","sys_time","sys_tty_config","mknod","lease","audit_write","audit_control","setfcap","mac_override","mac_admin","syslog","wake_alarm","block_suspend","audit_read"],"ambient":null}, "cwd": "/", "exe": "/usr/share/filebeat/bin/filebeat", "name": "filebeat", "pid": 101399, "ppid": 1, "seccomp": {"mode":"filter","no_new_privs":true}, "start_time": "2022-10-19T14:31:00.860+0300"}}}
2022-10-19T14:31:01.052+0300    INFO    instance/beat.go:273    Setup Beat: filebeat; Version: 6.4.0
2022-10-19T14:31:01.052+0300    INFO    pipeline/module.go:98   Beat name: host10
2022-10-19T14:31:01.053+0300    INFO    [monitoring]    log/log.go:114  Starting metrics logging every 30s
2022-10-19T14:31:01.053+0300    INFO    instance/beat.go:367    filebeat start running.
2022-10-19T14:31:01.053+0300    ERROR   beater/filebeat.go:315  Could not init registrar: Registry file path must be a file. /var/lib/filebeat/registry is a directory.
2022-10-19T14:31:01.054+0300    INFO    [monitoring]    log/log.go:149  Total non-zero metrics  {"monitoring": {"metrics": {"beat":{"cpu":{"system":{"ticks":0,"time":{"ms":8}},"total":{"ticks":10,"time":{"ms":18},"value":10},"user":{"ticks":10,"time":{"ms":10}}},"info":{"ephemeral_id":"0f3118ff-c42f-412f-995d-cd9d87c82789","uptime":{"ms":14}},"memstats":{"gc_next":4194304,"memory_alloc":2112448,"memory_total":3986768,"rss":25006080}},"filebeat":{"harvester":{"open_files":0,"running":0}},"libbeat":{"config":{"module":{"running":0}},"output":{"type":"kafka"},"pipeline":{"clients":0,"events":{"active":0}}},"registrar":{"states":{"current":0}},"system":{"cpu":{"cores":8},"load":{"1":0.03,"15":0,"5":0.02,"norm":{"1":0.0038,"15":0,"5":0.0025}}}}}}
2022-10-19T14:31:01.054+0300    INFO    [monitoring]    log/log.go:150  Uptime: 16.33053ms
2022-10-19T14:31:01.054+0300    INFO    [monitoring]    log/log.go:127  Stopping metrics logging.
2022-10-19T14:31:01.054+0300    INFO    instance/beat.go:373    filebeat stopped.
2022-10-19T14:31:01.056+0300    ERROR   instance/beat.go:743    Exiting: Registry file path must be a file. /var/lib/filebeat/registry is a directory.






[root@host10 filebeat]# cd /var/lib/filebeat/
[root@host10 filebeat]# ll
total 4
-rw------- 1 root root 100 Oct 19 12:27 meta.json
drwxr-x--- 3 root root  21 Oct 19 12:27 registry

##########################################################################
[root@host10 filebeat]# rm -rf registry
##########################################################################
[root@host10 filebeat]# systemctl restart filebeat
[root@host10 filebeat]# systemctl status filebeat
● filebeat.service - Filebeat sends log files to Logstash or directly to Elasticsearch.
   Loaded: loaded (/usr/lib/systemd/system/filebeat.service; enabled; vendor preset: disabled)
   Active: active (running) since Wed 2022-10-19 14:32:19 +03; 4s ago
     Docs: https://www.elastic.co/products/beats/filebeat
 Main PID: 101476 (filebeat)
    Tasks: 17
   CGroup: /system.slice/filebeat.service
           └─101476 /usr/share/filebeat/bin/filebeat -c /etc/filebeat/filebeat.yml -path.home /usr/share/filebeat -path.config /etc/filebeat -path.data /var/lib/filebeat -path.logs /var/log/filebeat

Oct 19 14:32:19 host10 systemd[1]: Started Filebeat sends log files to Logstash or directly to Elasticsearch..
