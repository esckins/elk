#####all index

`GET /_cat/indices?v&s=index`

##### twi* indexlerini getirir. (spans nodes)

```
GET /_cat/indices/twi*?v&s=index

health status index    uuid                   pri rep docs.count docs.deleted store.size pri.store.size
yellow open   twitter  u8FNjxh8Rfy_awN11oDKYQ   1   1       1200            0     88.1kb         88.1kb
green  open   twitter2 nYFWZEO7TUiOjLQXBaYJpA   5   0          0            0       260b           260b
```


##### yellow status index

```
GET /_cat/indices?v&health=yellow

health status index     uuid                   pri rep docs.count docs.deleted store.size pri.store.size
yellow open   testindex tcvSIxcKQMO2Zg-kz2ORHQ   3   2          0            0      1.2kb           624b
```

##### largest number of doc order by desc;

GET /_cat/indices?v&s=docs.count:desc

```
health status index    uuid                   pri rep docs.count docs.deleted store.size pri.store.size
yellow open   twitter  u8FNjxh8Rfy_awN11oDKYQ   1   1       1200            0     88.1kb         88.1kb
green  open   twitter2 nYFWZEO7TUiOjLQXBaYJpA   5   0          0            0       260b           260b
```

##### How many merge operations have the shards for the twitter completed?

```
GET /_cat/indices/twitter?pri&v&h=health,index,pri,rep,docs.count,mt

health index   pri rep docs.count mt pri.mt
yellow twitter   1   1 1200       16     16
```

##### How much memory is used per index?

```
GET /_cat/indices?v&h=i,tm&s=tm:desc

i         tm
twitter   8.1gb
twitter2  30.5kb
```

##### Important

```
GET _cat/indices/*?v&h=health,status,index,pri,docs.count,docs.deleted,store.size,pri.store.size

GET /_cat/indices?v=true&s=index

GET /_cat/indices/*iis-*?pretty=true&format=json  ##yaml,text

GET /_cat/indices?v&h=i,tm&s=tm:desc
```


##### nodes

```
GET /_cat/nodes?v

ip           heap.percent ram.percent cpu load_1m load_5m load_15m node.role master name
10.30.30.10            50          99  46    5.25    4.95     5.03 dilrt     -      elasticX01
10.30.30.11           57          99  53    4.73    4.71     5.00 dilrt     -      elasticX02
10.30.30.12            45          99  59    4.08    5.29     5.51 dilrt     -      elasticX03
10.30.30.13            66          99  54    5.26    4.22     3.93 dilrt     -      elasticX04
10.30.30.14           60          99  48    4.06    4.36     4.25 dilrt     -      elasticX05
10.30.30.15            71          99  43    2.23    3.38     4.12 dilrt     -      elasticX06
10.30.30.16           66          99  52    2.06    2.54     2.74 dilrt     -      elasticX19
10.30.30.17           25          99  44    4.07    3.78     3.30 dilrt     -      elasticX22
10.30.30.18           14          99  10    1.17    1.07     1.09 dilrt     -      elasticX24
10.30.30.19            33          99  63    3.14    4.35     4.53 dilrt     -      elastic14
10.30.30.20            49          99  38    3.42    4.70     5.57 dilrt     -      elastic03
10.30.30.21            64          99  27    3.26    3.96     4.16 dilmrt    -      elastic11
10.30.30.22            63          99  55    4.29    3.73     3.39 dilmrt    -      elastic13
10.30.30.23            72          99  27    1.88    2.17     2.65 dilmrt    *      elastic12


GET /_cat/nodes?v&h=id,ip,port,v,m
```


```
> curl -X GET "http://localhost:9200/_cat/indices?h=h,s,i,id,p,r,dc,dd,ss,creation.date.string"

green open elastiflow-4.0.1-2020.08.27             sVRYk9AMRGSjFD74g3nRYg 3 0 144971335      0  72.6gb 2020-08-27T00:00:21.123Z
green open elastiflow-4.0.1-2020.08.28             5Fw2GCxxQFC59A61V5LBSQ 3 0  90776646      0  45.2gb 2020-08-28T00:00:20.488Z
green open elastiflow-4.0.1-2020.08.25             WBTlCm3yREuN-xPiVSKz_g 3 0 151030419      0  74.5gb 2020-08-25T00:00:22.086Z
green open elastiflow-4.0.1-2020.08.26             EpM6F4kITA6YcIVql8PH6Q 3 0 150584878      0  74.2gb 2020-08-26T00:00:17.785Z
green open elastiflow-4.0.1-2020.08.24             GyWWbtTURSKcHe2eL6YeNg 3 0 150541062      0  74.5gb 2020-08-24T00:00:19.179Z
green open elastiflow-4.0.1-2020.08.18             z-B1XeTpTKWtJ-xNnvN6ig 3 0    209429      0 126.1mb 2020-08-18T13:49:14.933Z
```


[NODES](https://www.elastic.co/guide/en/elasticsearch/reference/5.6/cat-nodes.html)

##### Nots

_You can use the hot threads API to check where the CPU time is spent in Elasticsearch._


_No, but if you lose one master you lose the cluster._


_We strongly recommend maintaining a minimum of 30% free disk space in normal operations. This is to ensure there is sufficient available space for Elasticsearch to perform operations._

[resource_perform](https://www.instaclustr.com/support/documentation/elasticsearch/elasticsearch-monitoring/disk-usage/#:~:text=We%20strongly%20recommend%20maintaining%20a,for%20Elasticsearch%20to%20perform%20operations.)

#### Other

GET /_cat/health?v&ts=false



******************************************************************



```
GET _cluster/settings

{
  "persistent" : {
    "cluster" : {
      "routing" : {
        "allocation" : {
          "enable" : "all"
        }
      }
    },
    "indices" : {
      "breaker" : {
        "request" : {
          "limit" : "45%"
        }
      }
    },
    "xpack" : {
      "monitoring" : {
        "collection" : {
          "enabled" : "true"
        }
      }
    }
  },
  "transient" : {
    "cluster" : {
      "routing" : {
        "rebalance" : {
          "enable" : "all"
        },
        "allocation" : {
          "enable" : "all",
          "exclude" : {
            "_ip" : "10.30.30.10,10.30.30.30"
          }
        }
      }
    }
  }
}
```

```

PUT _cluster/settings
{
  "transient" : {
    "cluster.routing.allocation.exclude._ip" :""
  }
}

PUT _cluster/settings
{
  "transient" : {
    "cluster.routing.allocation.exclude._ip" :"10.30.30.30"
  }
}

```






********************************************************************

Elasticsearch Gateway timeout 504

It's normal if your index has a substantial size. You don't need to see any timeout, the task is still ongoing in the background.

You can check the status of the update by query task by running

GET _tasks?actions=*byquery&detailed


change the kibana.yml. add the line:

elasticsearch.requestTimeout: 90000  # default 30s


$ curl -XGET 'http://localhost:9200/_cat/count?v'

 count 416827938

 $ curl -XGET 'http://localhost:9200/_cat/nodes?v&s=name&h=master,name,version,node.role,disk.avail,heap.max,ram.max,ram.current,cpu,uptime,jdk'

$ curl -XGET 'http://localhost:9200/_cat/indices?v&s=index'

$ curl -XGET 'http://localhost:9200/_cat/shards?v&s=index'

$ curl -XGET 'http://localhost:9200/_cat/thread_pool/bulk?v&s=rejected&h=node_name,name,size,active,queue,rejected'



### when elasticsearch cluster master dead;

allocation temporarily throttled
unavailable shards exception

[unassigned-shards](https://stackoverflow.com/questions/19967472/elasticsearch-unassigned-shards-how-to-fix)

[alibaba configure monitoring](https://www.alibabacloud.com/help/en/doc-detail/68017.htm)

[max-shards](https://opster.com/guides/elasticsearch/operations/elasticsearch-max-shards-per-node-exceeded/)



```
GET _cluster/settings

GET _cluster/health

GET /_cat/shards

GET _cluster/state?pretty=true

GET /_cluster/health?pretty

GET /_cluster/allocation/explain?pretty

GET _cluster

GET /_cluster/allocation/explain

PUT _cluster/settings
{
  "transient" : {
    "cluster.routing.allocation.exclude._ip" :""
  }
}

GET /_cat/shards?h=index,shard,prirep,state,unassigned.reason

GET /_cluster/allocation/explain?pretty

GET /.kibana_task_manager*

PUT _cluster/settings
{
  "transient" : {
    "cluster.routing.allocation.exclude._ip" :"10.30.30.10"
  }
}


GET /_cat/shards?h=index,shard,prirep,state,unassigned.reason,node | grep -i unassigned


https://www.datadoghq.com/blog/elasticsearch-unassigned-shards/

curl -XGET localhost:9200/_cluster/allocation/explain?pretty
GET /_cluster/allocation/explain?pretty

{
  "note" : "No shard was specified in the explain API request, so this response explains a randomly chosen unassigned shard. There may be other unassigned shards in this cluster which cannot be assigned for different reasons. It may not be possible to assign this shard until one of the other shards is assigned correctly. To explain the allocation of other shards (whether assigned or unassigned) you must specify the target shard in the request to this API.",
  "index" : ".ds-.logs-deprecation.elasticsearch-default-2022.03.31-000008",
  "shard" : 0,
  "can_allocate" : "no_valid_shard_copy",
  "allocate_explanation" : "cannot allocate because a previous copy of the primary shard existed but can no longer be found on the nodes in the cluster",


GET _cat/shards/.ds-.logs-deprecation.elasticsearch-default-2022.03.31-000008?v
GET _cat/recovery/.ds-.logs-deprecation.elasticsearch-default-2022.03.31-000008?v


PUT /.ds-.logs-deprecation.elasticsearch-default-2022.03.31-000008/_settings
{
  "number_of_replicas": 0
}
lose a node from your cluster? -- cluster status Red

GET _cat/nodes?h=h,diskAvail

GET /_cat/indices/_all?v&s=store.size

GET _cat/indices/iis*,apm*,rest*,testvitapp*?pretty=true


GET _cluster/health

GET /_cluster/allocation/explain

GET /_cluster/health?wait_for_status=yellow&timeout=5s

GET _cat/shards?v&pretty

GET /_cat/indices?v

GET /_nodes/stats/fs

GET /_cat/indices?v&health=red

GET /_cat/nodes?v

GET /_cat/nodes?v&h=id,ip,port,v,m

GET /_cat/health?v&ts=false
```


#### when cluster monitoring not working

```
GET /.monitoring-es-7-2022.01.15/_search

GET /.monitoring-es-7-2022.01.16

DELETE /.monitoring-es-7-2022.01.16

POST _tasks/oTUltX4IQMOUUVeiohTt8A:12345/_cancel

POST /_cluster/reroute

POST _cluster/reroute?retry_failed=true

PUT _cluster/settings
{
  "transient" : {
   "cluster.routing.allocation.exclude._ip" : ""
  }
}

GET _cluster/stats

GET /_cat/thread_pool

GET /_cat/thread_pool/search?v&h=id,name,active,queue,rejected,completed
GET /_cat/thread_pool/write?v=true&h=id,name,active,rejected,completed,node

GET _cat/shards?v=true

GET /_cat/thread_pool/?v&h=id,name,active,rejected,completed,size,type&pretty&s=type

GET /_nodes/hot_threads
GET /_cluster/stats?human&pretty

grep 'processor' /proc/cpuinfo | sort -u | wc -l

GET /_cat/thread_pool?v
https://cdmana.com/2021/03/20210302113748193q.html

GET _nodes/stats/breaker

GET _cat/nodes?v=true&h=name,node*,heap*

GET _cat/nodes?v=true&s=cpu:desc

GET _nodes/10.30.30.10/hot_threads

GET /_cat/thread_pool?h=bulk*&v

GET _tasks?actions=*search&detailed

POST _tasks/oTUltX4IQMOUUVeiohTt8A:464/_cancel

GET _nodes/stats?filter_path=nodes.*.jvm.mem.pools.old

GET _nodes/stats?filter_path=nodes.*.jvm.mem.pools.old

GET _cluster/health?filter_path=status,*_shards

GET _cat/shards?v=true&h=index,shard,prirep,state,node,unassigned.reason&s=state

=node_name

GET /_cat/thread_pool?v=true&h=id,name,active,rejected,completed

GET /_cat/thread_pool?v&s=t,n&h=type,name,node_name,active,queue,rejected,completed

GET /_nodes/hot_threads

GET /_tasks?filter_path=nodes.*.tasks


GET _tasks?actions

GET _tasks?actions=*search&detailed

GET _tasks/oTUltX4IQMOUUVeiohTt8A:12345?wait_for_completion=true&timeout=10s



GET _tasks?actions=*reindex&wait_for_completion=true&timeout=10s

GET _tasks?actions=*search&detailed
{
  "nodes" : {
    "oTUltX4IQMOUUVeiohTt8A" : {
      "name" : "my-node",
      "transport_address" : "127.0.0.1:9300",
      "host" : "127.0.0.1",
      "ip" : "127.0.0.1:9300",
      "tasks" : {
      #################  "oTUltX4IQMOUUVeiohTt8A:464" : {
          "node" : "oTUltX4IQMOUUVeiohTt8A",
          "id" : 464,
          "type" : "transport",
          "action" : "indices:data/read/search",
          "description" : "indices[my-index], search_type[QUERY_THEN_FETCH], source[{\"query\":...}]",
      #################    "start_time_in_millis" : 4081771730000,
      #################    "running_time_in_nanos" : 13991383,
          "cancellable" : true
        }
      }
    }
  }
}
POST _tasks/oTUltX4IQMOUUVeiohTt8A:464/_cancel


GET _cluster/settings

{
  "persistent" : {
    "cluster" : {
      "routing" : {
        "allocation" : {
          "enable" : "all"
        }
      }
    },
    "indices" : {
      "breaker" : {
        "request" : {
          "limit" : "45%"
        }
      }
    },
    "xpack" : {
      "monitoring" : {
        "collection" : {
          "enabled" : "true"
        }
      }
    }
  },
  "transient" : {
    "cluster" : {
      "routing" : {
        "allocation" : {
          "include" : {
            "_ip" : ""
          },
          "exclude" : {
            "_ip" : ""
          }
        }
      }
    }
  }
}

PUT _cluster/settings
{
  "persistent" : {
    "cluster.routing.allocation.enable" : null
  }
}

GET _cat/allocation?v=true&h=node,shards,disk.*

POST _cluster/reroute

GET /_tasks?filter_path=nodes.*.tasks

GET /_nodes/hot_threads

GET _cluster/allocation/explain



DELETE /.monitoring-es-7-2022.05.16


PUT _cluster/settings
{
  "transient" : {
   "cluster.routing.allocation.exclude._ip" : ""
  }
}


GET _tasks 
GET _tasks?nodes=nodeId1,nodeId2 
GET _tasks?nodes=nodeId1,nodeId2&actions=cluster:*


GET _tasks/oTUltX4IQMOUUVeiohTt8A:124
GET _tasks?parent_task_id=oTUltX4IQMOUUVeiohTt8A:123

>Task hkk. detay getirir fakat risklidir.
GET _tasks?actions=*search&detailed



GET /_cat/indices/_all?v&s=store.size

GET /_cluster/health/?level=shards

GET _cat/shards?v&pretty

GET /_cat/indices?v

GET /_nodes/stats/fs

GET /_cat/indices?v&health=red

GET /_cat/nodes?v

GET /_cat/nodes?v&h=id,ip,port,v,m

GET /_cat/health?v&ts=false

GET /_cat/indices/twitter?pri&v&h=health,index,pri,rep,docs.count,mt


GET/.monitoring-es-7-2022.01.16/_search
{
  "size": 0,
  "query": {
    "term": {
      "type": "cluster_stats"
    }
  },
  "aggs": {
    "group_by_day": {
      "date_histogram": {
        "field": "timestamp",
        "interval": "day"
      }
    }
  }
}
```




### Fixing Red & Yellow Indexes

[resource]( https://steve-mushero.medium.com/elasticsearch-index-red-yellow-why-1c4a4a0256ca)

[resource2](https://opster.com/guides/elasticsearch/operations/elasticsearch-red-status/)

GET _cat/allocation?v

shards disk.indices disk.used disk.avail disk.total disk.percent host         ip           node
   382        2.2tb     2.2tb    758.8gb      2.9tb           75 
   383        2.2tb     2.2tb    746.1gb      2.9tb           75 
   384        2.1tb     2.1tb    831.7gb      2.9tb           72 
   382        2.2tb     2.2tb    683.7gb      2.9tb           77 
   241        1.9tb     1.9tb   1023.7gb      2.9tb           66 
   383        1.8tb     1.8tb        1tb      2.9tb           63 
   382        2.4tb     2.4tb    748.1gb      3.2tb           77 
   333        2.4tb     2.4tb    471.4gb      2.9tb           84 
   383        2.2tb     2.3tb    691.2gb      2.9tb           77 
   383          2tb     2.1tb    983.1gb        3tb           68 
   383        2.2tb     2.2tb        1tb      3.3tb           68 
   383        2.3tb     2.3tb    601.6gb      2.9tb           79 
   380        2.2tb     2.2tb    739.8gb      2.9tb           75 
   146        1.5tb     1.6tb      1.3tb      2.9tb           55 
   383        1.9tb     1.9tb    994.4gb      2.9tb           66 
   333        2.2tb     2.2tb    768.4gb      2.9tb           74 
   121      234.9gb   311.1gb      2.6tb      2.9tb           10 
   383        1.9tb     1.9tb        1tb      2.9tb           65 
   338        2.2tb     2.2tb      685gb      2.9tb           77 
   383        2.1tb     2.1tb    876.3gb      2.9tb           71 
   383        2.2tb     2.2tb    697.8gb      2.9tb           77 
   383        2.2tb     2.2tb    790.3gb      2.9tb           74 
   384          2tb       2tb    942.5gb      2.9tb           68 
    50      329.1gb     2.3tb    546.3gb      2.9tb           81 
   305                                                                                     UNASSIGNED


GET /_cat/indices?v&health=red

GET /_cat/indices?v&health=yellow

GET /_cat/shards?v&h=n,index,shard,prirep,state,sto,sc,unassigned.reason,unassigned.details&s=sto,index

GET /_cluster/allocation/explain

GET /_cluster/allocation/explain

"unassigned_info" : {
    "reason" : "NODE_LEFT",
    "at" : "2022-02-21T09:04:54.604Z",
    "details" : "node_left [ljtr6wYZRqqPgyTRlY0m6g]",
    "last_allocation_status" : "no_attempt"

curl -XGET localhost:9200/_cat/shards?h=index,shard,prirep,state,unassigned.reason| grep UNASSIGNED


I also encountered similar error. It happened to me because one of my data node was full and due to which shards allocation failed. If unassigned shards are there and your cluster is RED and few indices also RED, in that case I have followed below steps and these worked like a champ.
in kibana dev tool-

GET _cluster/allocation/explain

If any unassigned shards are there then you will get details else will throw ERROR.

simply running below command will solve everything-


```
PUT _cluster/settings
{
  "transient" : {
    "cluster.routing.allocation.exclude._ip" : ""
  }
}

unassigned shards;

POST _cluster/reroute?retry_failed
```


```
    "root_cause" : [
      {
        "type" : "illegal_argument_exception",
        "reason" : "No data for shard [0] of index [xyz] found on any node"
      }

curl -H 'Content-Type: application/json' \
    -XPOST '127.0.0.1:9200/_cluster/reroute?pretty' -d '{
    "commands" : [ {
        "allocate_empty_primary" :
            {
              "index" : "datauwu", "shard" : 6,
              "node" : "target-data-node-id",
              "accept_data_loss" : true
            }
        }
    ]
}'
```


```
The fourth step is to fix the problem. Fixes fall into a few categories:

Wait and let Elasticsearch fix it — For temporary conditions such as a node rebooting

Manually Allocate the Shard — Sometimes needed to fix things

Check Routing / Allocation Rules — Many HA or complex systems use routing or allocation rules to control placement, and as things change, this can create shards that can’t allocate. The explain should make this lear.

Remove all Replicas by setting number to 0 — Maybe you can’t fix the replica or manually move or assign it. In that case, as long as you have a primary (index is yellow, not red), you can always just set the replica count to 0, wait a minute, then set back to 1 or whatever you want, using: 

"index" : { "number_of_replicas" : 0 }
```

https://stackoverflow.com/questions/19967472/elasticsearch-unassigned-shards-how-to-fix

##Disk

https://www.elastic.co/guide/en/elasticsearch/reference/current/modules-cluster.html

cluster.routing.allocation.disk.threshold_enabled:true

```
PUT /_cluster/settings
{
  "transient": {
    "cluster.routing.allocation.disk.watermark.low": "90%",
    "cluster.routing.allocation.disk.watermark.high": "95%",
    "cluster.info.update.interval": "1m"
  }
}
```





****

```
GET _cat/indices?v&s=index

GET _cat/shards

GET /_nodes/info/jvm,process?pretty

GET /_nodes/NODES_1/hot_threads

GET _cluster/health

GET _cluster/settings

PUT _cluster/settings
{
  "transient" : {
    "cluster.routing.allocation.exclude._ip" :""
  }
}

https://admins.admins@10.30.30.10:8080
```


```
The maximum number of docs in a shard is 2^31, which is a lucene hard limit.
Otherwise we recommend around ~50GB, as larger can be a problem when reallocating/recovering.

However use cases can vary and our advise does evolve. So as always, test to see if that size is ok for your requirements.


***
Shards are "slices" of an index created by elasticsearch to have flexibility to distribute indexed data. For example, among several datanodes.

Shards, in the low level are independent sets of lucene segments that work autonomously, which can be queried independently. This makes possible the high performance because search operations can be split into independent processes.

The more shards you have the more flexible becomes the storage assignment for a given index. This obviously has some caveats.

Distributed searches must wait each other to merge step-results into a consistent response. If there are many shards, the query must be sliced into more parts, (which has a computing overhead). The query is distributed to each shard, whose hashes match any of the current search (not all shards are necesary hit by every query) therefore the most busy (slower) shard, will define the overall performance of your search.

It's better to have a balanced number of indexes. Each index has a memory footprint that is stored in the cluster state. The more indexes you have the bigger the cluster state, the more time it takes to be shared among all cluster nodes. The more shards an index has, the complexer it becomes, therefore the size taken to serialize it into the cluster state is bigger, slowing things down globally.

***

There are no hard limits on shard size, but experience shows that shards between 10GB and 50GB typically work well for logs and time series data. You may be able to use larger shards depending on your network and use case. Smaller shards may be appropriate for Enterprise Search and similar use cases.

`https://www.elastic.co/guide/en/elasticsearch/reference/current/size-your-shards.html#:~:text=There%20are%20no%20hard%20limits,Search%20and%20similar%20use%20cases.`


```

#### authentication

```
GET _opendistro/_security/api/rolesmapping

{
  "readall_and_monitor" : {
    "hosts" : [ ],
    "users" : [ ],
    "reserved" : false,
    "hidden" : false,
    "backend_roles" : [ ],
    "and_backend_roles" : [ ]
  },
  "readall_and_monitor_vakifbank" : {
    "hosts" : [ ],
    "users" : [
      "elasticmonitor",
      "usr_catplatform",
      "entuser",
      "operasyon"
    ],
    "reserved" : false,
    "hidden" : false,
    "backend_roles" : [
      "gB*"
    ],
    "and_backend_roles" : [ ]
  },
  "own_index" : {
    "hosts" : [ ],
    "users" : [
      "*"
    ],
    "reserved" : false,
    "hidden" : false,
    "backend_roles" : [ ],
    "and_backend_roles" : [ ],
    "description" : "Allow full access to an index named like the username"
  },
  "kibana_user" : {
    "hosts" : [ ],
    "users" : [ ],
    "reserved" : false,
    "hidden" : false,
    "backend_roles" : [
      "kibanax"
    ],
    "and_backend_roles" : [ ],
    "description" : "Maps kibanax to kibana_user"
  },
  "all_access" : {
    "hosts" : [ ],
    "users" : [
      "admin1",
      "entuser"
    ],
    "reserved" : false,
    "hidden" : false,
    "backend_roles" : [
      "SAUser",
      "admin"
    ],
    "and_backend_roles" : [ ]
  },
  "Logstash_All" : {
    "hosts" : [ ],
    "users" : [
      "logstash"
    ],
    "reserved" : false,
    "hidden" : false,
    "backend_roles" : [ ],
    "and_backend_roles" : [ ]
  },
  "manage_snapshots_copy" : {
    "hosts" : [ ],
    "users" : [
      "admin1"
    ],
    "reserved" : false,
    "hidden" : false,
    "backend_roles" : [ ],
    "and_backend_roles" : [ ]
  },
  "readall" : {
    "hosts" : [ ],
    "users" : [ ],
    "reserved" : false,
    "hidden" : false,
    "backend_roles" : [
      "readall"
    ],
    "and_backend_roles" : [ ]
  },
  "manage_snapshots" : {
    "hosts" : [ ],
    "users" : [ ],
    "reserved" : false,
    "hidden" : false,
    "backend_roles" : [
      "snapshotrestore"
    ],
    "and_backend_roles" : [ ]
  },
  "logstash" : {
    "hosts" : [ ],
    "users" : [ ],
    "reserved" : false,
    "hidden" : false,
    "backend_roles" : [
      "logstash"
    ],
    "and_backend_roles" : [ ]
  },
  "logstash_copy" : {
    "hosts" : [ ],
    "users" : [ ],
    "reserved" : false,
    "hidden" : false,
    "backend_roles" : [
      "logstash"
    ],
    "and_backend_roles" : [ ]
  },
  "security_rest_api_access" : {
    "hosts" : [ ],
    "users" : [
      "admin1"
    ],
    "reserved" : false,
    "hidden" : false,
    "backend_roles" : [ ],
    "and_backend_roles" : [ ]
  },
  "eses" : {
    "hosts" : [ ],
    "users" : [
      "automicmsy"
    ],
    "reserved" : false,
    "hidden" : false,
    "backend_roles" : [ ],
    "and_backend_roles" : [ ]
  },
  "kibana_read_only_custom" : {
    "hosts" : [ ],
    "users" : [
      "test"
    ],
    "reserved" : false,
    "hidden" : false,
    "backend_roles" : [ ],
    "and_backend_roles" : [ ]
  },
  "kibana_read_only_all_menu" : {
    "hosts" : [ ],
    "users" : [
      "test",
      "sh12214"
    ],
    "reserved" : false,
    "hidden" : false,
    "backend_roles" : [
      "gB*"
    ],
    "and_backend_roles" : [ ]
  },
  "siber" : {
    "hosts" : [ ],
    "users" : [
      "sh10112"
    ],
    "reserved" : false,
    "hidden" : false,
    "backend_roles" : [
      "SAUser"
    ],
    "and_backend_roles" : [ ]
  },
  "kibana_server" : {
    "hosts" : [ ],
    "users" : [
      "kibanax"
    ],
    "reserved" : true,
    "hidden" : false,
    "backend_roles" : [ ],
    "and_backend_roles" : [ ]
  },
  "readall_and_monitor_custom" : {
    "hosts" : [ ],
    "users" : [
      "elasticx"
    ],
    "reserved" : false,
    "hidden" : false,
    "backend_roles" : [ ],
    "and_backend_roles" : [ ]
  }
}
```

```
GET _opendistro/_security/api/internalusers

{
  "logstash" : {
    "hash" : "",
    "reserved" : false,
    "hidden" : false,
    "backend_roles" : [
      "logstash"
    ],
    "attributes" : { },
    "description" : "Demo logstash user",
    "opendistro_security_roles" : [ ],
    "static" : false
  },
  "entuser" : {
    "hash" : "",
    "reserved" : false,
    "hidden" : false,
    "backend_roles" : [ ],
    "attributes" : { },
    "opendistro_security_roles" : [ ],
    "static" : false
  },
  "elasticmonitor" : {
    "hash" : "",
    "reserved" : false,
    "hidden" : false,
    "backend_roles" : [ ],
    "attributes" : { },
    "opendistro_security_roles" : [ ],
    "static" : false
  },
  "kibanaro" : {
    "hash" : "",
    "reserved" : false,
    "hidden" : false,
    "backend_roles" : [
      "kibanx",
      "readall"
    ],
    "attributes" : {
      "attribute1" : "value1",
      "attribute2" : "value2",
      "attribute3" : "value3"
    },
    "description" : "Demo kibanaro user",
    "opendistro_security_roles" : [ ],
    "static" : false
  },
  "readall" : {
    "hash" : "",
    "reserved" : false,
    "hidden" : false,
    "backend_roles" : [
      "readall"
    ],
    "attributes" : { },
    "description" : "Demo readall user",
    "opendistro_security_roles" : [ ],
    "static" : false
  }
}
```
```
PUT _opendistro/_security/api/internalusers/<username>
{
  "password": "kirk",
  "roles": ["captains", "starfleet"],
   "attributes": {
     "attribute1": "value1",
     "attribute2": "value2",       
   }
}
{
  "status":"CREATED",
  "message":"User kirk created"
}
```

`GET _opendistro/_security/authinfo\?pretty`


**Unable to update UI setting Request failed with status code: 403**


`POST request to https://kibana:5601/s/test/api/kibana/settings with this payload {"changes":{"defaultIndex":"cdb47b30-ee5c-11e9-a95d-d5689c2d15d1"}}.`




`GET /api/security/v1/users/_customer_test`



```
{
    "username": "_customer_test",
    "roles": [
        "_customer_test"
    ],
    "full_name": "Test Industries Limited",
    "email": "",
    "metadata": {},
    "enabled": true
}
```

`GET /api/security/role/_customer_test`


```
{
    "name": "_customer_test",
    "metadata": {},
    "transient_metadata": {
        "enabled": true
    },
    "elasticsearch": {
        "cluster": [],
        "indices": [
            {
                "names": [
                    "test-*"
                ],
                "privileges": [
                    "read",
                    "view_index_metadata"
                ],
                "allow_restricted_indices": false
            }
        ],
        "run_as": []
    },
    "kibana": [
        {
            "base": [],
            "feature": {
                "discover": [
                    "all"
                ],
                "visualize": [
                    "all"
                ],
                "dashboard": [
                    "all"
                ],
                "maps": [
                    "all"
                ],
                "canvas": [
                    "all"
                ]
            },
            "spaces": [
                "test"
            ]
        }
    ],
    "_transform_error": [],
    "_unrecognized_applications": []
}
```

[issues](https://github.com/elastic/kibana/issues/46124)



```
GET .kibana/_search/?size=10000
{"_source": 
{"includes": ["_id","index-pattern.title"]},
"query": {
"term": 
{"type": "index-pattern"}
}
}
```

`curl -s -k -X GET "${kibanaurl}/api/saved_object/_find?fields=id&type=dashboard&per_page=1000" -u ${user}:${password}  | jq '.saved_objects[].id' | xargs -n1 -I{} curl -s -k -X GET "${kibanaurl}/api/kibana/dashboards/export?dashboard={}" -u ${user}:${password} -o {}.json`

```
GET <kibana host>:<port>/api/saved_objects/_find

GET <kibana host>:<port>/s/<space_id>/api/saved_objects/_find

curl -X GET "localhost:5601/api/kibana/dashboards/export?dashboard=942dcef0-b2cd-11e8-ad8e-85441f0c2e5c" -H 'kbn-xsrf: true'
```

```
POST _ingest/pipeline/_simulate  
{  
  "pipeline": {  
  "description" : "parse multiple patterns",  
  "processors": [   
    {   
      "grok": {     
        "field": "message",  
        "patterns": [ "CLIENT_ID=%{NOTSPACE:client_value}" ]   
           }   
    }   
  ]    
  },   
"docs":[   
  {   
    "_source": {   
      "message": "2020-07-29 03:59:19,393 -0700 INFO [http-nio-8080-exec-2139] abchohfowhofnfnnfnwlnflw CLIENT_ID=MNOPQR xysbxs"    
    }     
  }      
  ]   
}       



And the result is 

{
  "docs" : [
    {
      "doc" : {
        "_index" : "_index",
        "_type" : "_doc",
        "_id" : "_id",
        "_source" : {
          "message" : "2020-07-29 03:59:19,393 -0700 INFO [http-nio-8080-exec-2139] abchohfowhofnfnnfnwlnflw CLIENT_ID=MNOPQR xysbxs",
          "client_value" : "MNOPQR"
        },
        "_ingest" : {
          "timestamp" : "2020-07-29T18:25:29.07763Z"
        }     
      }
    }
  ]
}

enter code here


```

```

[2022-07-17T02:13:31,302][WARN ][o.e.t.TransportService   ] [ELKX20] Received response for a request that has timed out, sent [11643ms] ago, timed out [0ms]
ago, action [internal:coordination/fault_detection/leader_check], node [{ELKX12}{aSp0IPdQT9yi03ExBT97Gg}{IbNca3JZQVOcdqHrUJID5A}{10.30.69.37}{10.30.69.37:930
0}{dilmrt}{ml.machine_memory=29195218944, ml.max_open_jobs=20, xpack.installed=true, transform.node=true}], id [115331946]
[2022-07-17T02:13:31,472][ERROR][o.e.x.m.c.n.NodeStatsCollector] [ELKX20] collector [node_stats] timed out when collecting data
[2022-07-17T02:14:11,151][WARN ][o.e.t.TransportService   ] [ELKX20] Received response for a request that has timed out, sent [28755ms] ago, timed out [10894
ms] ago, action [internal:coordination/fault_detection/leader_check], node [{ELKX12}{aSp0IPdQT9yi03ExBT97Gg}{IbNca3JZQVOcdqHrUJID5A}{10.30.69.37}{10.30.69.37
:9300}{dilmrt}{ml.machine_memory=29195218944, ml.max_open_jobs=20, xpack.installed=true, transform.node=true}], id [115333601]
[2022-07-17T02:14:11,770][ERROR][o.e.x.m.c.n.NodeStatsCollector] [ELKX20] collector [node_stats] timed out when collecting data
[2022-07-17T02:15:31,443][ERROR][o.e.x.m.c.n.NodeStatsCollector] [ELKX20] collector [node_stats] timed out when collecting data
[2022-07-17T02:15:57,222][ERROR][o.e.x.m.c.n.NodeStatsCollector] [ELKX20] collector [node_stats] timed out when collecting data


----

Your problem is that you have far too many shards, which makes gathering cluster information slow. Having lots of small shards is very inefficient so you will need to reduce this dramatically. Please read this blog post 131 for guidance.
If getting node statistics time out it is possible that other cluster operations like creating new indices or relocating shards may be slow too which could cause problems.


https://www.elastic.co/blog/how-many-shards-should-i-have-in-my-elasticsearch-cluster




[2022-07-16T23:26:27,079][ERROR][o.e.x.m.c.n.NodeStatsCollector] [ELKX06] collector [node_stats] timed out when collecting data
[2022-07-16T23:26:35,599][INFO ][o.e.i.e.I.EngineMergeScheduler] [ELKX06] [iislog-2d-2022.28][11] now throttling indexing: numMergesInFlight=10, maxNumMerges=9
[2022-07-16T23:26:37,906][INFO ][o.e.i.e.I.EngineMergeScheduler] [ELKX06] [vitapp-2-2022.28][6] now throttling indexing: numMergesInFlight=10, maxNumMerges=9
[2022-07-16T23:26:46,173][ERROR][o.e.x.m.c.n.NodeStatsCollector] [ELKX06] collector [node_stats] timed out when collecting data
[2022-07-16T23:27:06,675][ERROR][o.e.x.m.c.n.NodeStatsCollector] [ELKX06] collector [node_stats] timed out when collecting data
[2022-07-16T23:27:29,549][INFO ][o.e.x.m.p.NativeController] [ELKX06] Native controller process has stopped - no new native processes can be started
[2022-07-16T23:27:31,871][ERROR][o.e.x.m.c.n.NodeStatsCollector] [ELKX06] collector [node_stats] timed out when collecting data

---
I don’t think Native controller process has stopped - no new native processes can be started is the problem. That’s an info message that is getting logged because the Elasticsearch JVM is dying for some other reason.

In the last 10 lines of the log you tailed I can see the end of a stack trace. Probably that relates to the reason the JVM is dying. Look at the beginning of that stack trace and the messages immediately before it. They will probably hint at the problem that needs to be fixed.


---

Job for elasticsearch.service failed because the control process exited with error code. See "systemctl status elasticsearch.service" and "journalctl -xe" for details.
sudo chown -R elasticsearch:elasticsearch /etc/elasticsearch/

journalctl -xe

Jul 17 17:19:29 ELKX06 performance-analyzer-agent-cli[48072]: at java.base/sun.nio.fs.UnixFileSystemProvider.newByteChannel(UnixFileSystemProvider.java:219)
Jul 17 17:19:29 ELKX06 performance-analyzer-agent-cli[48072]: at java.base/java.nio.file.Files.newByteChannel(Files.java:375)
Jul 17 17:19:29 ELKX06 performance-analyzer-agent-cli[48072]: at java.base/java.nio.file.Files.newByteChannel(Files.java:426)
Jul 17 17:19:29 ELKX06 performance-analyzer-agent-cli[48072]: at java.base/java.nio.file.spi.FileSystemProvider.newInputStream(FileSystemProvider.java:420)
Jul 17 17:19:29 ELKX06 performance-analyzer-agent-cli[48072]: at java.base/java.nio.file.Files.newInputStream(Files.java:160)
Jul 17 17:19:29 ELKX06 performance-analyzer-agent-cli[48072]: at java.base/java.util.Scanner.<init>(Scanner.java:718)
Jul 17 17:19:29 ELKX06 performance-analyzer-agent-cli[48072]: at com.amazon.opendistro.elasticsearch.performanceanalyzer.rca.RcaController.lambda$readRcaEnab
Jul 17 17:19:29 ELKX06 performance-analyzer-agent-cli[48072]: at com.amazon.opendistro.elasticsearch.performanceanalyzer.core.Util.lambda$invokePrivileged$1(
Jul 17 17:19:29 ELKX06 performance-analyzer-agent-cli[48072]: at java.base/java.security.AccessController.doPrivileged(AccessController.java:312)
Jul 17 17:19:29 ELKX06 performance-analyzer-agent-cli[48072]: at com.amazon.opendistro.elasticsearch.performanceanalyzer.core.Util.invokePrivileged(Util.java
Jul 17 17:19:29 ELKX06 performance-analyzer-agent-cli[48072]: at com.amazon.opendistro.elasticsearch.performanceanalyzer.rca.RcaController.readRcaEnabledFrom
Jul 17 17:19:29 ELKX06 performance-analyzer-agent-cli[48072]: at com.amazon.opendistro.elasticsearch.performanceanalyzer.rca.RcaController.run(RcaController.
Jul 17 17:19:29 ELKX06 performance-analyzer-agent-cli[48072]: at com.amazon.opendistro.elasticsearch.performanceanalyzer.PerformanceAnalyzerApp.lambda$startR
Jul 17 17:19:29 ELKX06 performance-analyzer-agent-cli[48072]: at com.amazon.opendistro.elasticsearch.performanceanalyzer.threads.ThreadProvider.lambda$create
Jul 17 17:19:29 ELKX06 performance-analyzer-agent-cli[48072]: at java.base/java.lang.Thread.run(Thread.java:832)
Jul 17 17:19:29 ELKX06 performance-analyzer-agent-cli[48072]: 17:19:29.691 [pa-reader] ERROR com.amazon.opendistro.elasticsearch.performanceanalyzer.reader.R
Jul 17 17:19:32 ELKX06 performance-analyzer-agent-cli[48072]: Jul 17, 2022 5:19:32 PM org.jooq.tools.JooqLogger info
Jul 17 17:19:32 ELKX06 performance-analyzer-agent-cli[48072]: INFO: Single batch             : No bind variables have been provided with a single statement b
Jul 17 17:19:32 ELKX06 performance-analyzer-agent-cli[48072]: Jul 17, 2022 5:19:32 PM org.jooq.tools.JooqLogger info
Jul 17 17:19:32 ELKX06 performance-analyzer-agent-cli[48072]: INFO: Single batch             : No bind variables have been provided with a single statement b
Jul 17 17:19:32 ELKX06 performance-analyzer-agent-cli[48072]: Jul 17, 2022 5:19:32 PM org.jooq.tools.JooqLogger info
Jul 17 17:19:32 ELKX06 performance-analyzer-agent-cli[48072]: INFO: Single batch             : No bind variables have been provided with a single statement b
Jul 17 17:19:32 ELKX06 performance-analyzer-agent-cli[48072]: 17:19:32.254 [pa-reader] ERROR com.amazon.opendistro.elasticsearch.performanceanalyzer.reader.R
Jul 17 17:19:34 ELKX06 performance-analyzer-agent-cli[48072]: 17:19:34.108 [rca-controller] ERROR com.amazon.opendistro.elasticsearch.performanceanalyzer.rca
Jul 17 17:19:34 ELKX06 performance-analyzer-agent-cli[48072]: java.nio.file.NoSuchFileException: /usr/share/elasticsearch/data/rca_enabled.conf
Jul 17 17:19:34 ELKX06 performance-analyzer-agent-cli[48072]: at java.base/sun.nio.fs.UnixException.translateToIOException(UnixException.java:92)
Jul 17 17:19:34 ELKX06 performance-analyzer-agent-cli[48072]: at java.base/sun.nio.fs.UnixException.rethrowAsIOException(UnixException.java:111)
Jul 17 17:19:34 ELKX06 performance-analyzer-agent-cli[48072]: at java.base/sun.nio.fs.UnixException.rethrowAsIOException(UnixException.java:116)
Jul 17 17:19:34 ELKX06 performance-analyzer-agent-cli[48072]: at java.base/sun.nio.fs.UnixFileSystemProvider.newByteChannel(UnixFileSystemProvider.java:219)
Jul 17 17:19:34 ELKX06 performance-analyzer-agent-cli[48072]: at java.base/java.nio.file.Files.newByteChannel(Files.java:375)
Jul 17 17:19:34 ELKX06 performance-analyzer-agent-cli[48072]: at java.base/java.nio.file.Files.newByteChannel(Files.java:426)
Jul 17 17:19:34 ELKX06 performance-analyzer-agent-cli[48072]: at java.base/java.nio.file.spi.FileSystemProvider.newInputStream(FileSystemProvider.java:420)
Jul 17 17:19:34 ELKX06 performance-analyzer-agent-cli[48072]: at java.base/java.nio.file.Files.newInputStream(Files.java:160)
Jul 17 17:19:34 ELKX06 performance-analyzer-agent-cli[48072]: at java.base/java.util.Scanner.<init>(Scanner.java:718)
Jul 17 17:19:34 ELKX06 performance-analyzer-agent-cli[48072]: at com.amazon.opendistro.elasticsearch.performanceanalyzer.rca.RcaController.lambda$readRcaEnab
Jul 17 17:19:34 ELKX06 performance-analyzer-agent-cli[48072]: at com.amazon.opendistro.elasticsearch.performanceanalyzer.core.Util.lambda$invokePrivileged$1(
Jul 17 17:19:34 ELKX06 performance-analyzer-agent-cli[48072]: at java.base/java.security.AccessController.doPrivileged(AccessController.java:312)
Jul 17 17:19:34 ELKX06 performance-analyzer-agent-cli[48072]: at com.amazon.opendistro.elasticsearch.performanceanalyzer.core.Util.invokePrivileged(Util.java
Jul 17 17:19:34 ELKX06 performance-analyzer-agent-cli[48072]: at com.amazon.opendistro.elasticsearch.performanceanalyzer.rca.RcaController.readRcaEnabledFrom
Jul 17 17:19:34 ELKX06 performance-analyzer-agent-cli[48072]: at com.amazon.opendistro.elasticsearch.performanceanalyzer.rca.RcaController.run(RcaController.
Jul 17 17:19:34 ELKX06 performance-analyzer-agent-cli[48072]: at com.amazon.opendistro.elasticsearch.performanceanalyzer.PerformanceAnalyzerApp.lambda$startR
Jul 17 17:19:34 ELKX06 performance-analyzer-agent-cli[48072]: at com.amazon.opendistro.elasticsearch.performanceanalyzer.threads.ThreadProvider.lambda$create
Jul 17 17:19:34 ELKX06 performance-analyzer-agent-cli[48072]: at java.base/java.lang.Thread.run(Thread.java:832)
Jul 17 17:19:34 ELKX06 performance-analyzer-agent-cli[48072]: 17:19:34.709 [pa-reader] ERROR com.amazon.opendistro.elasticsearch.performanceanalyzer.reader.R


netstat -a -n | grep tcp | grep 9200

If nothing shows up, it may mean elasticsearch was not started or failed to start, check the log
If something shows up, you can tell which IP it is bound to then test that one with the curl command. If it works, update kibana's configuration file (I think that's what you have in the screenshot)
check  hs_err_pid49435.log , gc.log

systemctl stop metricbeat

systemctl start metricbeat

GET _cluster/allocation/explain
GET /_cluster/settings
GET /_cat/shards
$ curator --host 172.16.4.140 delete indices --older-than 1 \
       --timestring '%Y.%m.%d' --time-unit days --prefix logstash

curl -XGET http://localhost:9200/_cat/shards |\
  grep UNASSIGNED | grep ' r ' |\
  awk '{print $1}' |\
  xargs -I {} curl -XPUT http://localhost:9200/{}/_settings -H "Content-Type: application/json" \
  -d '{ "index":{ "number_of_replicas": 0}}'

curl -XGET http://localhost:9200/_cat/shards |\
  awk '{print $1}' |\
  xargs -I {} curl -XPUT http://localhost:9200/{}/_settings -H "Content-Type: application/json" \
  -d '{ "index":{ "number_of_replicas": 1}}'

PUT _cluster/settings
{
  "persistent": {
    "cluster.routing.allocation.disk.threshold_enabled": "false"
  }
}

https://stackoverflow.com/questions/19967472/elasticsearch-unassigned-shards-how-to-fix
POST _cluster/reroute?retry_failed


---
#!/usr/bin/env bash

# The script performs force relocation of all unassigned shards, 
# of all indices to a specified node (NODE variable)

ES_HOST="<elasticsearch host>"
NODE="<node name>"

curl ${ES_HOST}:9200/_cat/shards > shards
grep "UNASSIGNED" shards > unassigned_shards

while read LINE; do
  IFS=" " read -r -a ARRAY <<< "$LINE"
  INDEX=${ARRAY[0]}
  SHARD=${ARRAY[1]}

  echo "Relocating:"
  echo "Index: ${INDEX}"
  echo "Shard: ${SHARD}"
  echo "To node: ${NODE}"

  curl -s -XPOST "${ES_HOST}:9200/_cluster/reroute" -d "{
    \"commands\": [
       {
         \"allocate\": {
           \"index\": \"${INDEX}\",
           \"shard\": ${SHARD},
           \"node\": \"${NODE}\",
           \"allow_primary\": true
         }
       }
     ]
  }"; echo
  echo "------------------------------"
done <unassigned_shards

rm shards
rm unassigned_shards

exit 0



Caused by: org.elasticsearch.common.util.concurrent.EsRejectedExecutionException: rejected execution of org.elasticsearch.action.support.replication.TransportWrite
Action$1@5ccc1589 on EsThreadPoolExecutor[name = ISMLAELSLX09/write, queue capacity = 500, org.elasticsearch.common.util.concurrent.EsThreadPoolExecutor@53a492a8[R
unning, pool size = 10, active threads = 10, queued tasks = 613, completed tasks = 39450597]]


thread_pool.search.queue_size increase 

https://www.elastic.co/guide/en/elasticsearch/reference/current/modules-threadpool.html



----

Usually, when it happens to the search queue to fill up it means elasticsearch is not having the needed throughput to attend all your requests. Increasing the search queue only makes the problem happens a little later. It's a producer/consumer problem.

To solve this you have some alternatives:

Add more resources to your cluster (upgrade hardware, or add more nodes)
Optimize your queries
Perform fewer requests
etc
Hope it helps.

--> Last day,I set ' thread_pool.search.min_queue_size: 2000 ' on elasticsearch.yml and it's work for me. Rejected thread pools are decrease.



Well, it basically means that you've got 1000 search requests that have queued up waiting to run, and once the limit is reached ES just starts aborting new requests.

So you'll need to figure out the bottleneck. Some options:

Your clients are simply sending too many queries too quickly in a fast burst, overwhelming the queue. You can monitor this with Node Stats over time to see if it's bursty or smooth
You've got some very slow queries which get "stuck" for a long time, eating up threads and causing the queue to back up. You can enable the slow log to see if there are queries that are taking an exceptionally long time, then try to tune those
There may potentially be "unending" scripts written in Groovy or something. E.g. a loop that never exits, causing the thread to spin forever.
Your hardware may be under-provisioned for your workload, and bottlenecking on some resource (disk, cpu, etc)
A temporary hiccup from your iSCSI target, which causes all the in-flight operations to block waiting for the disks to come back. It wouldn't take a big latency hiccup to seriously backup a busy cluster... ES generally expects disks to always be available.
Heavy garbage collections could cause problems too. Check Node Stats to see if there are many/long old gen GCs running


---

search thread_pool has an associated queue on which search worker threads work on and its default capacity is 1000(note its per node) which is consumed in your case, please refer thread_pools in ES for more info.

Now, queue capacity can be exhausted due to one of the below reasons:

High search requests rate on your ES node, which your node might not be able to keep up with.
Some slow queries which are taking a lot more time to process, and causing other requests to wait in the search queue, and ultimately leading to rejection.
Your nodes and shards are properly balanced in your cluster, this can cause few nodes to receive a high number of search queries.
You should look at the search slow-logs to filter the costly queries during the issue.

And if its legitimate increase in search traffic, you should scale your cluster to handle more search requests.

Regarding young heap logs, if it's not taking more time and under 50-70ms its not that bigger concern, and you should focus on fixing the search rejection issue which might solve or lower the heap issue and in any case you should have Max heap size lower than 32 GB for better performance.

---

One important thing to note is that, if you got these stats from elasticsearch threadpool cat API then it shows just the point-in-time data and doesn't show the historical data for the last 1 hr, 6 hr, 1 day, 1 week like that.

And rejected and completed is the stats from the last restart of the nodes, so this is also not very helpful when we are trying to figure out if some of ES nodes are becoming hot-spots due to bad/unbalanced shards configuration.

So here we have two very important things to figure out

Make sure, we know the actual hotspot nodes in the cluster by looking at the average active, rejected requests on data nodes by time range(you can just check for peak hours).
Once hotspot nodes are known, look at the shards allocated to them, and compare it to other nodes shards, few metric to check is, number of shards, shards receive more traffic, shards receive slowest queries, etc and again most of them you have to figure out by looking at various metrics and API of ES which can be very time consuming and requires a lot of internal ES knowledge.

```

```
 curl -X GET 'http://localhost:9200/(index)/_search'?pretty=true
 curl -X GET 'http://localhost:9200/_cat/indices?v' -u elastic:(password)

 curl --user $pwd  -H 'Content-Type: application/json' -XGET https://58571402f5464923883e7be42a037917.eu-central-1.aws.cloud.es.io:9243/_cluster/health?pretty

 backup
 curl -XPOST --header 'Content-Type: application/json' http://localhost:9200/_reindex -d '{
"source": {
"index": "samples"
},
"dest": {
"index": "samples_backup"
}
}'

curl -X GET 'http://localhost:9200/_cat/indices?v'

GET /_cat/indices?v

GET /_cat/count?v

```
```
503 problem ;

# curl -u kibanaserver:kibanaserver -XGET https://elastic.esckin.intra/status -I -H 'Content-Type: application/json'
HTTP/1.1 200 OK
content-type: text/html; charset=utf-8
content-security-policy: script-src 'unsafe-eval' 'self'; worker-src blob: 'self'; style-src 'unsafe-inline' 'self'
kbn-name: kibana
kbn-license-sig: xca
kbn-xpack-sig: asc
cache-control: private, no-cache, no-store, must-revalidate
set-cookie: security_authentication=Fe26.2**s2213; HttpOnly; Path=/
content-length: 231
vary: accept-encoding
accept-ranges: bytes
date: Tue, 11 Oct 2022 14:53:22 GMT





$ kubectl describe deployment opendistro-elastic-production-kibana-deployment -n esckin
Name:                   opendistro-elastic-production-kibana-deployment
Namespace:              esckin
CreationTimestamp:      Thu, 07 Jan 2021 10:57:47 +0300
Labels:                 app=kibana
                        for=opendistro-elastic-production-kibana-deployment
                        version=7.9.1
Annotations:            deployment.kubernetes.io/revision: 6
Selector:               app=kibana,for=opendistro-elastic-production-kibana-deployment,version=7.9.1
Replicas:               1 desired | 1 updated | 1 total | 1 available | 0 unavailable
StrategyType:           RollingUpdate
MinReadySeconds:        0
RollingUpdateStrategy:  25% max unavailable, 25% max surge
Pod Template:
  Labels:  app=kibana
           for=opendistro-elastic-production-kibana-deployment
           version=7.9.1
  Containers:
   opendistro-elastic-production-kibana-deployment:
    Image:        harbor.esckin.intra/uty/kibana:v7.9.1
    Port:         5601/TCP
    Host Port:    0/TCP
    Readiness:    http-get http://:5601/api/status delay=20s timeout=5s period=10s #success=1 #failure=3
    Environment:  <none>
    Mounts:
      /usr/share/kibana/config from kibana-config-volume (rw)
  Volumes:
   kibana-config-volume:
    Type:      ConfigMap (a volume populated by a ConfigMap)
    Name:      kibana-opendistro-config
    Optional:  false
Conditions:
  Type           Status  Reason
  ----           ------  ------
  Progressing    True    NewReplicaSetAvailable
  Available      True    MinimumReplicasAvailable
OldReplicaSets:  <none>
NewReplicaSet:   opendistro-elastic-production-kibana-deployment-757dbc7129 (1/1 replicas created)
Events:          <none>


kubectl edit deployment vs ...

/_cat/nodes?v=true

in pod ;
/usr/share/kibana/config

 curl -u esckin:<<password>> -X GET "http://10.10.30.30:9200/_cluster/health?pretty" -H 'Content-Type: application/json'
{
  "cluster_name" : "vakifcluster",
  "status" : "green",
  "timed_out" : false,
  "number_of_nodes" : 34,
  "number_of_data_nodes" : 34,
  "active_primary_shards" : 5625,
  "active_shards" : 11282,
  "relocating_shards" : 1,
  "initializing_shards" : 0,
  "unassigned_shards" : 0,
  "delayed_unassigned_shards" : 0,
  "number_of_pending_tasks" : 0,
  "number_of_in_flight_fetch" : 0,
  "task_max_waiting_in_queue_millis" : 0,
  "active_shards_p..

/usr/share/kibana/bin
./bin/kibana 


$ cat kibana.yml
server.name: kibana
server.host: "0.0.0.0"
elasticsearch.hosts: ["http://10.10.30.30:9200","http://10.10.30.31:9200"]
xpack.security.enabled: false
elasticsearch.ssl.verificationMode: none
elasticsearch.username: kibanaserver
elasticsearch.password: kibanaserver
elasticsearch.requestHeadersWhitelist: ["securitytenant","Authorization"]
opendistro_security.multitenancy.enabled: false
opendistro_security.multitenancy.tenants.preferred: ["Private", "Global"]
opendistro_security.readonly_mode.roles: ["kibana_read_only"]
#opendistro_security.auth.type: "jwt"




kubectl get pods -n esckin
 kubectl exec -it -n esckin opendistro-elastic-production-kibana-deployment-757dc775c92sdj5  bash
kubectl logs opendistro-elastic-production-kibana-deployment-757dc775c92sdj5 -n esckin

kubectl describe deployment opendistro-elastic-production-kibana-deployment -n esckin

/usr/share/kibana/config

warning
Readiness probe failed: Get http://10.30.10.30:5601/api/status: dial tcp 10.1.103.01:5601: connect: connection refused
kubelet esckinsx05
spec.containers{opendistro-elastic-production-kibana-deployment}
22022-10-11T12:44 UTC2022-10-11T12:44 UTC
warning
Readiness probe failed: HTTP probe failed with statuscode: 503
kubelet esckinsx05
spec.containers{opendistro-elastic-production-kibana-deployment}
572022-10-11T12:44 UTC2022-10-11T12:54 UTC



curl -s http://<IP_ADDRESS>:5601/app/kibana#/dashboard/API\ RESPONSES
curl -s http://<IP_ADDRESS>:5601/app/kibana#/dashboard/logs
curl -s http://<IP_ADDRESS>:5601/app/kibana#/dashboard/notifications

curl -s http://<IP_ADDRESS>:9200/.kibana/dashboard/_search?pretty

From <https://stackoverflow.com/questions/65648100/how-to-curl-kibana-dashboard-with-get-response-to-find-all-dashboard-ids-and-or> 


too many request;

https://stackoverflow.com/questions/50609417/elasticsearch-error-cluster-block-exception-forbidden-12-index-read-only-all


curl -XPUT -H "Content-Type: application/json" http://localhost:9200/_cluster/settings -d '{ "transient": { "cluster.routing.allocation.disk.threshold_enabled": false } }'
curl -XPUT -H "Content-Type: application/json" http://localhost:9200/_all/_settings -d '{"index.blocks.read_only_allow_delete": null}'

Solution 1: free up disk space
$ curl -XPUT -H "Content-Type: application/json" https://[YOUR_ELASTICSEARCH_ENDPOINT]:9200/_all/_settings -d '{"index.blocks.read_only_allow_delete": null}'



Solution 2: change the flood stage watermark setting
PUT _cluster/settings
{
  "transient": {
    "cluster.routing.allocation.disk.watermark.low": "100gb",
    "cluster.routing.allocation.disk.watermark.high": "50gb",
    "cluster.routing.allocation.disk.watermark.flood_stage": "10gb",
    "cluster.info.update.interval": "1m"
  }
}







a. Create a json file with following parameters


{
  "persistent": {
    "cluster.routing.allocation.disk.watermark.low": "90%",
    "cluster.routing.allocation.disk.watermark.high": "95%",
    "cluster.routing.allocation.disk.watermark.flood_stage": "97%"
  }
}

b.Name it anything ,e.g : json.txt

c.Type following command in command prompt

>curl -X PUT "localhost:9200/_cluster/settings?pretty" -H "Content-Type: application/json" -d @json.txt

d.Following output is received.

{
  "acknowledged" : true,
  "persistent" : {
    "cluster" : {
      "routing" : {
        "allocation" : {
          "disk" : {
            "watermark" : {
              "low" : "90%",
              "flood_stage" : "97%",
              "high" : "95%"
            }
          }
        }
      }
    }
  },
  "transient" : { }
}

e.Create another json file with following parameter


{
  "index.blocks.read_only_allow_delete": null
}

f.Name it anything ,e.g : json1.txt

g.Type following command in command prompt

>curl -X PUT "localhost:9200/*/_settings?expand_wildcards=all" -H "Content-Type: application/json" -d @json1.txt

h.You should get following output

{"acknowledged":true}

i.Restart ELK stack/Kibana and the issue should be resolved.




```



Useful Links ;

[jvm settings](https://www.elastic.co/guide/en/logstash/current/jvm-settings.html)

[jvm Options](https://www.elastic.co/guide/en/elasticsearch/reference/master/advanced-configuration.html#set-jvm-options)


[ClusterHealth](https://www.elastic.co/guide/en/elasticsearch/reference/current/cluster-health.html)

[catApi](https://www.elastic.co/blog/introducing-cat-api)

[open distro](https://opendistro.github.io/for-elasticsearch-docs/old/0.9.0/docs/security/api/#get-authentication-details)
