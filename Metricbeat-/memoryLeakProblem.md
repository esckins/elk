Problem is memory leak due to metricbeat.

![image.png](./image.png)

upgrade version metricbeat but not fixed problem.My solution is wa. Restarted by hourly via cronjob

```
# crontab -l | tail -1
@hourly /bin/systemctl restart metricbeat.service
```

scp metricbeat-8.2.0-x86_64.rpm root@esckinx02:/tmp
scp metricbeat-7.12.01-x86_64.rpm root@esckinx02:/tmp
rpm -e metricbeat-7.15.1-1.x86_64

rm -rf /etc/metricbeat

cd /tmp

rpm -ivh metricbeat-8.2.0-x86_64.rpm

cd /etc/metricbeat

output.kafka:
  hosts: ["kafkax01:9600","kafkax02:9600","kafkalx02:9600"]
  topic: 'esckins'
  partition.round_robin:
    reachable_only: true

  required_acks: 1
  compression: gzip
  max_message_bytes: 1000000

```
  
systemctl enable metricbeat
```


```
# Module: system
# Docs: https://www.elastic.co/guide/en/beats/metricbeat/8.2/metricbeat-module-system.html

- module: system
  period: 10s
  metricsets:
    - cpu
    - load
    - memory
    - network
    - process
    - process_summary
    - socket_summary
    - core
    - diskio
    - users
    - load
  enabled: true
  process.include_top_n:
    by_cpu: 10      # include top 5 processes by CPU
    by_memory: 10   # include top 5 processes by memory
# Configure the mount point of the host’s filesystem for use in monitoring a host from within a container
# hostfs: "/hostfs"

- module: system
  period: 10m
  metricsets:
    - filesystem
    - fsstat
  enabled: true
  processors:
  - drop_event.when.regexp:
      system.filesystem.mount_point: '^/(sys|cgroup|proc|dev|etc|host|lib|snap)($|/)'

#- module: system
#  period: 5m
#  metricsets:
#    - raid
#  raid.mount_point: '/'
```

