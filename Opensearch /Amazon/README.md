amazon vs elasticsearch :) result is opensearch

sample;
vi docker-compose.yml

```
version: '3'
services:
  opensearch-node1:
    image: opensearchproject/opensearch:1.3.1
    container_name: opensearch-node1
    environment:
      - cluster.name=opensearch-cluster
      - node.name=opensearch-node1
      - bootstrap.memory_lock=true # along with the memlock settings below, disables swapping
      - "OPENSEARCH_JAVA_OPTS=-Xms512m -Xmx512m" # minimum and maximum Java heap size, recommend setting both to 50% of system RAM
      - "DISABLE_INSTALL_DEMO_CONFIG=true" # disables execution of install_demo_configuration.sh bundled with security plugin, which installs demo certificates and security configurations to OpenSearch
      - "DISABLE_SECURITY_PLUGIN=true" # disables security plugin entirely in OpenSearch by setting plugins.security.disabled: true in opensearch.yml
      - "discovery.type=single-node" # disables bootstrap checks that are enabled when network.host is set to a non-loopback address
    ulimits:
      memlock:
        soft: -1
        hard: -1
      nofile:
        soft: 65536 # maximum number of open files for the OpenSearch user, set to at least 65536 on modern systems
        hard: 65536
    volumes:
      - opensearch-data1:/usr/share/opensearch/data
    ports:
      - 9200:9200
      - 9600:9600 # required for Performance Analyzer
    networks:
      - opensearch-net

  opensearch-dashboards:
    image: opensearchproject/opensearch-dashboards:1.3.0
    container_name: opensearch-dashboards
    ports:
      - 5601:5601
    expose:
      - "5601"
    environment:
      - 'OPENSEARCH_HOSTS=["http://opensearch-node1:9200"]'
      - "DISABLE_SECURITY_DASHBOARDS_PLUGIN=true" # disables security dashboards plugin in OpenSearch Dashboards
    networks:
      - opensearch-net

volumes:
  opensearch-data1:

networks:
  opensearch-net:
```

docker-compose up

[installOpensearch](https://opensearch.org/docs/latest/opensearch/install/docker/)


[Reports](https://opensearch.org/docs/1.3/dashboards/reporting/#create-reports-using-a-definition)
 ----  [Chronium](https://github.com/opensearch-project/dashboards-reports/releases/tag/chromium-1.12.0.0)


 
![Opensearch](opensearchDashboard.png)



![exPNG Dash.](sss_2022-05-01T23_38_01.759Z_bd7f22f0-c9a7-11ec-81e7-2d19dd75dbe0.png)



![exPDF Dash.](test_2022-05-01T23_29_40.461Z_92b321d0-c9a6-11ec-81e7-2d19dd75dbe0.pdf)



