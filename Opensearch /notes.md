Thanks for the open source support.

it is very nice to use these features like that anomaly detection, reporting, alerting etc., it will get perfect as it develops. 

[Opensearch](https://opensearch.org/docs/latest/opensearch/index/)

currently;
Dont send info notification channel like that slack,mail etc.

[issues](https://github.com/opensearch-project/dashboards-reports/issues/18)

observability


[Kurulum](https://iamdual.com/posts/opensearch-kurulum/)


Bu yazıda OpenSearch’ten ve onun kurulumundan, ayrıca Elasticsearch ile arasındaki bağlantıdan söz edeceğim. Kullanımı hakkında daha fazla bilgi için Elasticsearch ile ilgili yazdığım bu yazıya göz atabilirsiniz.

Öncelikle Elasticsearch neydi; kayıtların yüksek performanslı olarak aranabilmesini sağlayan NoSQL bir veritabanıydı. Elasticsearch, ücretsiz olmasının yanı sıra ücretli commercial çözümler de sunan bir şirkettir aynı zamanda ve şirketin ticari amaçlı olarak hizmet verdiğini de unutmamak gerekir.

Elasticsearch tek başına ücretsiz de olsa, barındırdığı birçok plugin ve kendisine entegre edilebilecek diğer yazılımlar için lisans ücreti ödenmesi gerekmektedir. Şirket, 2021 yılında bir karar alarak, açık kaynak lisansıyla dağıttığı yazılımlarının 7.11 ve sonrasındaki sürümleri için Apache 2.0 yerine Server Side Public License (SSPL) kullanacağını belirtmiştir. SSPL, Open Source Initiative komitesi tarafından “açık kaynak” ünvanı için fazla kısıtlayıcı bulunduğu için kabul edilmemiştir. Ayrıca, bulut servis sağlayıcılarının yazılımı bir servis olarak kullanıcılara sunulmasını da kısıtlamaktaydı. Elasticsearch, Amazon Web Services (AWS) tarafından hali hazırda bir hizmet olarak sunulduğu için bu durum AWS’nin hiç hoşuna gitmemişti. Böylece OpenSearch isimli yeni bir projenin adımları atıldı. OpenSearch, Elasticsearch 7.10.2 versiyonunun forku ile oluşturulmuştur.

Şimdi kurulumuna geçebiliriz. OpenSearch, kendi sitesinde de anlattığı gibi Docker ile çok kolay bir şekilde kurulabiliyor. Fakat bunun yerine tarball kurulumunu isterseniz yazının devamında detaylıca anlattım.

Buna OpenSearch’ün resmi sitesine girerek başlayalım. Anasayfada “Get Started” veya “Download” linkine tıkladıktan sonra platformunuza uygun tarball’un linkini kopyalayın.

Linux / 64-bit için olan örnekte, kopyaladığımız dosya linkinden sunucuya indiriyoruz.

```
wget "https://artifacts.opensearch.org/releases/bundle/opensearch/1.2.1/opensearch-1.2.1-linux-x64.tar.gz"
Şimdi dosyaları /opt/opensearch içerisine çıkartalım.

mkdir /opt/opensearch
tar -zxf opensearch-1.2.1-linux-x64.tar.gz -C /opt/opensearch --strip-components=1
```

OpenSearch (ve Elasticsearch), yazılımın root kullanıcısıyla çalıştırılmasını engellemekte. Bu yüzden OpenSearch’ü çalıştırabilmesi için kullanıcı oluşturuyoruz.

```
groupadd opensearch
useradd opensearch -g opensearch --shell=/bin/false
Gerekli izinleri veriyoruz.

chown -R opensearch:opensearch /opt/opensearch
Son olarak geri kalan işlemlere opensearch kullanıcı ile devam ediyoruz.

su - opensearch
cd /opt/opensearch
Burada öncelikle, SSL kurulumu ile uğraşmamak adına security eklentisini devre dışı bırakıyorum.

nano config/opensearch.yml
Değeri true olarak değiştirip kaydediyoruz.

plugins.security.disabled: true
```

Şimdi ./bin/opensearch komutunu çalıştırdığımızda hiçbir hata vermeden OpenSeach’ün çalışması lazım. Hatta curl komutuyla bu esnada test yapabilirsiniz de.

```
curl http://localhost:9200
{
  "name" : "Ubuntu-2004-focal-64-minimal",
  "cluster_name" : "opensearch",
  "cluster_uuid" : "RbwNIsqPTWWLCIlE7nIqwA",
  "version" : {
    "distribution" : "opensearch",
    "number" : "1.1.0",
    "build_type" : "tar",
    "build_hash" : "15e9f137622d878b79103df8f82d78d782b686a1",
    "build_date" : "2021-10-04T21:29:03.079792Z",
    "build_snapshot" : false,
    "lucene_version" : "8.9.0",
    "minimum_wire_compatibility_version" : "6.8.0",
    "minimum_index_compatibility_version" : "6.0.0-beta1"
  },
  "tagline" : "The OpenSearch Project: https://opensearch.org/"
}
```

Burada da bir sorun meydana gelmediyse, servis oluşturma kaldı geriye. Bu yazıda servis oluşturmak için gerekli detaylı açıklamalar mevcuttur.

Aşağıda OpenSearch’e özel oluşturulmuş olan servis dosyasını bulabilirsiniz:

```
[Unit]
Description=OpenSearch
Wants=network-online.target
After=network-online.target

[Service]
Type=simple
User=opensearch
Group=opensearch
WorkingDirectory=/opt/opensearch/
Environment=OPENSEARCH_HOME=/opt/opensearch/
Environment=JAVA_HOME=/opt/opensearch/jdk/
ExecStart=/opt/opensearch/bin/opensearch -p ${OPENSEARCH_HOME}/opensearch.pid --quiet
StandardOutput=journal
StandardError=inherit
LimitNOFILE=65536
LimitMEMLOCK=infinity
TimeoutStopSec=0
# SIGTERM sinyali Java işlemini durdurmak için kullanılıyor
KillSignal=SIGTERM
# Sinyali onun kontrol grubu yerine JVMye gönder
KillMode=process
# Java işlemi sonlandırılamaz
SendSIGKILL=no
# JVM SIGTERM sinyali alırsa 143 exit kodu döndürür
SuccessExitStatus=143

[Install]
WantedBy=multi-user.target
```
