#!/bin/python3
#!/usr/bin/python3
import os
import re
from elasticsearch import Elasticsearch
import sys
import numpy as np
import time
import json
import base64
import requests
import logging

#logging.basicConfig(filename='new_indices.log', level=logging.WARNING)
#logging.debug('This message should go to the log file')
#logging.warning('And this, too')


#test---esckinsx01
#prod---esckinsx10

es = Elasticsearch(
    ['esckinsx01'],
    scheme="http",
    port=9200,
    http_auth=('admin', 'admin'),
    timeout=60
)


all_indices = ['.apm-agent-configuration', '.apm-custom-link', '.async-search', '.kibana-event-log-7.15.0-000001', '.kibana-event-log-7.15.0-000002', '.kibana-event-log-7.15.0-000003', '.kibana-event-log-7.15.0-000004', '.kibana-event-log-7.9.2-000015', '.kibana-event-log-7.9.2-000016', '.kibana-event-log-7.9.2-000017', '.kibana-event-log-7.9.2-000018', '.kibana_1', '.kibana_7.15.0_001', '.kibana_task_manager_1', '.kibana_task_manager_7.15.0_001', '.monitoring-es-7-2022.03.04', '.monitoring-es-7-2022.03.05', '.monitoring-es-7-2022.03.06', '.monitoring-es-7-2022.03.07', '.monitoring-es-7-2022.03.08', '.monitoring-es-7-2022.03.09', '.monitoring-es-7-2022.03.10', '.monitoring-kibana-7-2022.03.04', '.monitoring-kibana-7-2022.03.05', '.monitoring-kibana-7-2022.03.06', '.monitoring-kibana-7-2022.03.07', '.monitoring-kibana-7-2022.03.08', '.monitoring-kibana-7-2022.03.09', '.monitoring-kibana-7-2022.03.10', '.tasks', 'bitcoin-2022.02.04', 'btc']

new_list = es.cat.indices(h='index', s='index').split()

for i in range(len(all_indices)):
    all_indices[i] = all_indices[i].replace('-', ' ').split(' ')[0]

for i in range(len(new_list)):
    new_list[i] = new_list[i].replace('-', ' ').split(' ')[0]


s1 = set(all_indices)
s2 = set(new_list)


new_index = []
for i in list(s2 - s1):
    if i not in all_indices:
        #print(i)
        new_index.append(i)
print(new_index)
logging.basicConfig(filename='new_indices.log', level=logging.WARNING)
#logging.debug('This message should go to the log file')
logging.warning(new_index)
