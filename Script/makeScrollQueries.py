#https://kb.objectrocket.com/elasticsearch/how-to-use-python-to-make-scroll-queries-to-get-all-documents-in-an-elasticsearch-index-752

#!/bin/python3
#!usr/bin/python3
import os
import re
from elasticsearch import Elasticsearch
import sys
import numpy as np
import time
import json
import base64
import requests


#####################################

#!/usr/bin/env python3
#-*- coding: utf-8 -*-

# import the Elasticsearch client library
from elasticsearch import Elasticsearch, exceptions

# import JSON and time
import json, time

# create a timestamp using the time() method
start_time = time.time()

# declare globals for the Elasticsearch client host
DOMAIN = "localhost"
PORT = 9200

# concatenate a string for the client's host paramater
host = str(DOMAIN) + ":" + str(PORT)

# declare an instance of the Elasticsearch library
client = Elasticsearch(host)

try:
    # use the JSON library's dump() method for indentation
    info = json.dumps(client.info(), indent=4)

    # pass client object to info() method
    print ("Elasticsearch client info():", info)

except exceptions.ConnectionError as err:

    # print ConnectionError for Elasticsearch
    print ("\nElasticsearch info() ERROR:", err)
    print ("\nThe client host:", host, "is invalid or cluster is not running")

    # change the client's value to 'None' if ConnectionError
    client = None

# valid client instance for Elasticsearch
if client != None:

    # get all of the indices on the Elasticsearch cluster
    all_indices = client.indices.get_alias("*")

    # keep track of the number of the documents returned
    doc_count = 0

    # iterate over the list of Elasticsearch indices
    for num, index in enumerate(all_indices):

        # declare a filter query dict object
        match_all = {
            "size": 100,
            "query": {
                "match_all": {}
            }
        }

        # make a search() request to get all docs in the index
        resp = client.search(
            index = index,
            body = match_all,
            scroll = '2s' # length of time to keep search context
        )

        # keep track of pass scroll _id
        old_scroll_id = resp['_scroll_id']

        # use a 'while' iterator to loop over document 'hits'
        while len(resp['hits']['hits']):

            # make a request using the Scroll API
            resp = client.scroll(
                scroll_id = old_scroll_id,
                scroll = '2s' # length of time to keep search context
            )

            # check if there's a new scroll ID
            if old_scroll_id != resp['_scroll_id']:
                print ("NEW SCROLL ID:", resp['_scroll_id'])

            # keep track of pass scroll _id
            old_scroll_id = resp['_scroll_id']

            # print the response results
            print ("\nresponse for index:", index)
            print ("_scroll_id:", resp['_scroll_id'])
            print ('response["hits"]["total"]["value"]:', resp["hits"]["total"]["value"])

            # iterate over the document hits for each 'scroll'
            for doc in resp['hits']['hits']:
                print ("\n", doc['_id'], doc['_source'])
                doc_count += 1
                print ("DOC COUNT:", doc_count)

    # print the total time and document count at the end
    print ("\nTOTAL DOC COUNT:", doc_count)

# print the elapsed time
print ("TOTAL TIME:", time.time() - start_time, "seconds.")
