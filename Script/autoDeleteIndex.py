#!/bin/python3
#!usr/bin/python3

import datetime
import requests
import json
import os

logfile = 'delete_api_log'

# read file
with open('user.json', 'r') as myfile:
    data = myfile.read()

#print(data)
dict1 = json.loads(data)


def write_log(logfilename, logmessage):
    file_object = open(logfilename, 'a', encoding='utf-8')
    file_object.write(logmessage+"\n")
    file_object.close()

def stringToList(string):
    listRes = list(string.split(" "))
    return listRes


def read_json(file_path):
    with open(file_path, "r") as f:
        return json.load(f)


dt = datetime.date.today()
wk = dt.isocalendar()[1]
year = dt.isocalendar()[0]

print(wk)
print(dt)

#this week is $week
#week=datetime.datetime.utcnow().isocalendar()[1]
#print(week)

print('Today is {0} , week is {1} and then year is {2}'.format(dt,wk,year))
print()
print(f'datetimeToday: {dt} weak: {wk} year: {year}')


url_base = str(dict1[0]["Schema"]) + str(dict1[0]["Username"]) + ":" + str(dict1[0]["Password"]) + "@" + str(dict1[0]["Server"]) + ":" + str(dict1[0]["Port"])+"/"
response_url = url_base + "_cat/indices?pretty=true"
#response_url = url_base + "_cat/indices/iis-*?pretty=true"
#response_url = url_base + "_cat/indices?h=index&format=json"
response = requests.get(response_url)



print(dict1[0]["Schema"])
print(response_url)
print(response)


#if response.status_code == 200:
print('---------Success!')
a = stringToList(response.text)
#print(a)
print(type(a))

new_list = []
search_list = []
##wk=52
i = wk-9

print('i degeri {0}'.format(i))

while i > 1:
    if i < 10:
      SEARCHTEXT = '2021.' + '0' + str(i)
      search_list.append(SEARCHTEXT)
      i -=1
    else:
      SEARCHTEXT = '2021.' + str(i)
      search_list.append(SEARCHTEXT)
      i -= 1
    print(SEARCHTEXT)

#print()
#print('yeni i degeri {0}'.format(i))


for i in range(len(search_list)):
    for item in a:
        if search_list[i] in item:
           new_list.append(item)
        else:
           continue

#print(f'item is: {item}')

result = [x for x in new_list if not x.startswith('.monitoring')]

print(f'result is: {result}')
#print(type(result))

started_time = datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S.%f')
start_message = "Starting at " + str(started_time)
write_log(logfile + "-" + str(year) + ".log", start_message)

for i in range(len(result)):
    try:
        url_used = url_base + str(result[i])
        print(url_used)
        response = requests.get(url_used, timeout=5)
        #response = requests.delete(url_used, timeout=10)
        elps_time = response.elapsed
        message = str(url_used)+" " + str(elps_time)
    except:
        message = str(url_used) + "Bağlanılamadı"

    write_log(logfile + "-" + str(year) + ".log", message)

finished_time = datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S.%f')
write_log(logfile + "-" + str(year) + ".log", "Finished at " + str(finished_time) + '\n\n')
