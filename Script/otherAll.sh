#!/bin/bash

ELKSERVER=${1:-localhost}
ELKPORT=${2:-9200}
if [ -z "$ELKSERVER" ]; then
  # Usage
  echo 'Usage: check-health.sh <elk-server=localhost> <elk-port=9200>'
  # create-index.sh "logstash-solr-*" 
else
  curl -s "http://$ELKSERVER:$ELKPORT/_cat/health?v"
fi
check-kibana-status.sh
#!/bin/bash
KIBANAMODE=${1:-"-b"}
KIBANASERVER=${2:-localhost}
KIBANAPORT=${3:-5601}
if [ -z "$KIBANASERVER" ]; then
  # Usage
  echo 'Usage: check-kibana-status.sh <-b|-f> [<kibana-server=localhost> <kibana-port=5601>]'
else
  if [ "$KIBANAMODE" = "-b" ]; then
    curl -s "http://$KIBANASERVER:$KIBANAPORT/api/status" | jq ".status.overall"
  else
    curl -s "http://$KIBANASERVER:$KIBANAPORT/api/status" | jq ".status.statuses"
  fi
fi
check-kibana.sh
#!/bin/bash

SERVER=${1:-localhost}
PORT=${2:-5601}
VAR=${3:-status}
WARNING=${4:-1}
CRITICAL=${5:-2}

if [ "$PORT" = "443" ]; then
   PROTOCOL="https" 
else
   PROTOCOL="http" 
fi

# Endpoint for Kibana API status 
ENDPOINT="$PROTOCOL://${SERVER}:${PORT}/api/status"

if [[ "$1" == "" ]]; then
  echo "USAGE:"
  echo "  check_kibana.sh <SERVER> <PORT> <VAR=status|load|heap> <WARNING> <CRITICAL>"
  exit
fi

CURL=`curl --silent -X GET ${ENDPOINT}`
CHCK=`echo $CURL | grep "$VAR"`

if [[ "$CHCK" == "" ]]; then
   CHECK="Failed"
else
   CHECK="OK"
fi

if [[ "$CHECK" == "OK" ]]; then
   if [[ "$VAR" == "status" ]]; then
      MYDIV=`echo $CURL | jq ".status.overall.state"`
      if [[ "$MYDIV" == "\"green\"" ]]; then
        echo "INFO: Kibana ($VAR) = $MYDIV"
        exit 0
      else
        echo "WARN: Kibana ($VAR) = $MYDIV"
        exit 1
      fi
   fi
   if [[ "$VAR" == "load" ]]; then
      # Kibana 6
      MYDIV=`echo $CURL | jq ".metrics.os.cpu.load_average" | tr '\n' ' ' | sed -e 's/  / /g'`
      # Kibana 5
      #MYDIV=`echo $CURL | jshon -e "metrics" | jshon -e "load" | jshon -e 0 | jshon -e 1 | tr '\n' ' '`
      echo "INFO: Kibana ($VAR) = $MYDIV"
      exit 0
   fi  
   if [[ "$VAR" == "heap" ]]; then
      # Kibana 6
      MYDIV=`echo $CURL | jq ".metrics.process.mem" | tr '\n' ' ' | sed -e 's/  / /g'`
      echo "INFO: Kibana ($VAR) = $MYDIV"
      exit 0
   fi  
   # Kibana 5 
   #MYDIV=`echo $CURL | jshon -e "metrics" | jshon -e "$VAR" | jshon -e 0 | jshon -e 1`
   #echo "INFO: Kibana ($VAR) = $MYDIV"
   #exit 0
elif [[ "$CHECK" == "Failed" ]]; then
   echo "CRITICAL: ${SERVER}"
   exit 2
else
   echo "Check failed."
   exit 3
fi
create-index-pattern.sh
#!/bin/bash
KIBANASERVER=${2:-localhost}
KIBANAPORT=${3:-5601}
if [ -z "$1" ]; then
  # Usage
  echo 'Usage: create-index-pattern.sh <index-name="logstash-*">'
  # create-index.sh "logstash-solr-*" 
else
  echo "Creating index pattern $1 in Kibana"

  curl -XPOST -D- "http://$KIBANASERVER:$KIBANAPORT/api/saved_objects/index-pattern" \
    -H 'Content-Type: application/json' \
    -H 'kbn-version: 6.1.0' \
    -d "{\"attributes\":{\"title\":\"$1\",\"timeFieldName\":\"@timestamp\"}}"
fi
delete-index.sh
#!/bin/bash
ELKSERVER=${2:-localhost}
ELKPORT=${3:-9200}

if [ -z "$1" ]; then
  # Usage
  echo 'Usage: delete-index.sh <index-name="logstash-2017.10.23">'
else
  echo "Deleting index pattern $1 in Elastic Search"
  curl -s -XDELETE "http://$ELKSERVER:$ELKPORT/$1"
fi
do-el-search.sh
#!/bin/bash
QUERY=${1:-*:*}
ELKINDEX=${2:-_all}
ROWS=${3:-10}
FL=${4:-host,message,@timestamp}
ELKSERVER=${5:-localhost}
ELKPORT=${6:-9200}

if [ -z "$1" ]; then
  # Usage
  echo 'Usage: do-search.sh <q=*:*> <index-name="logstash-2017.10.23"> <rows=10> <fl=host,message,@timestamp>'
else
  COMMAND=`curl -s "http://$ELKSERVER:$ELKPORT/$ELKINDEX/_search?q=$QUERY&size=$ROWS&_source=$FL&pretty=true"`
  echo $COMMAND | jq
  echo "Number of results for search: $QUERY" 
  echo $COMMAND | jq ".hits.total"
fi
get-mapping.sh
#!/bin/bash
ELKSERVER=${2:-localhost}
ELKPORT=${3:-9200}

if [ -z "$1" ]; then
  # Usage
  echo 'Usage: get-mapping.sh <index-name="logstash-2017.10.23">'
else
  curl -s http://$ELKSERVER:$ELKPORT/$1/_mapping | jq ".\"$1\".mappings.doc.properties"| jshon -k 
fi
list-indices.sh
#!/bin/bash

ELKMODE=${1:-"-f"}
ELKSERVER=${2:-localhost}
ELKPORT=${3:-9200}
if [ -z "$ELKMODE" ]; then
  # Usage
  echo 'Usage: list-indices.sh <-b|-f> [<elk-server=localhost> <elk-port=9200>]'
  # create-index.sh "logstash-solr-*" 
else
  if [ "$ELKMODE" = "-b" ]; then
    curl -s "http://$ELKSERVER:$ELKPORT/_cat/indices?v" | awk '{print $3}' | grep -v "index" | sort
  else
    curl -s "http://$ELKSERVER:$ELKPORT/_cat/indices?v"
  fi
fi
