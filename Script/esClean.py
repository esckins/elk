https://gist.github.com/egonbraun

#!/usr/bin/env python

import time
import json
import re
import base64
import requests

# #############################################################################

# The minimum allowed percetage usage for our storage. If the current value
# is greater than the one informed here then we will delete indexes
STORAGE_USAGE_MIN_THRESHOLD=75.0

# User and password to access the ElasticSearch API
PROXY_USER='user'
PROXY_PASSWORD='password'

# Information used to build the URL used to access the ElasticSearch API and
# create the new snapshot
PROXY_PROTO='https'
PROXY_HOST='proxy-elasticsearch.com'
PROXY_PATH_GET_ALL_INDEXES='_cluster/health?level=indices'
PROXY_PATH_GET_STORAGE='_cluster/stats?human&pretty'

# The prefix string for the indexes names
INDEX_PREFIX='logstash-'

# #############################################################################

# The actual URL we will use in order to get all indexes
PROXY_URL_GET_INDEXES='%s://%s/%s' % (
    PROXY_PROTO,
    PROXY_HOST,
    PROXY_PATH_GET_ALL_INDEXES,
)

# The actual URL we will use in order to delete the indexes
PROXY_URL_DELETE_INDEXES='%s://%s' % (
    PROXY_PROTO,
    PROXY_HOST
)

# The actual URL we will use in order to get storage usages
PROXY_URL_GET_STORAGE='%s://%s/%s' % (
    PROXY_PROTO,
    PROXY_HOST,
    PROXY_PATH_GET_STORAGE
)

# #############################################################################

def lambda_handler(event, context):
    print 'INFO: Lambda handler activated'
    print 'INFO: Event ID is %s' % event['id']

    main()

# #############################################################################

def get_storage_usage():
    print 'INFO: Getting storage size'
    print 'INFO: URL=%s' % PROXY_URL_GET_STORAGE

    headers = {
        'Content-Type': 'application/json',
    }

    try:
        req = requests.get(
            PROXY_URL_GET_STORAGE,
            headers=headers,
            auth=(PROXY_USER, PROXY_PASSWORD)
        )
    except requests.exceptions.ConnectionError as e:
        print 'ERROR: Not able to connect to URL'
        return -1
    except requests.exceptions.Timeout as e:
        print 'ERROR: ElasticSearch Timeout'
        return -1
    except requests.exceptions.HTTPError as e:
        print 'ERROR: HTTP Error'
        return -1
    else:
        print 'INFO: ElasticSearch Response Code = %s' % req.status_code

        if req.status_code != 200:
            return -1
        else:
            data = req.json()['nodes']['fs']
            free_storage  = float(data['free_in_bytes'])
            total_storage = float(data['total_in_bytes'])

            return 100.0 - ((free_storage / total_storage) * 100.0)

# #############################################################################

def get_indexes():
    print 'INFO: Getting list of indexes'
    print 'INFO: URL=%s' % PROXY_URL_GET_INDEXES

    headers = {
        'Content-Type': 'application/json',
    }

    try:
        req = requests.get(
            PROXY_URL_GET_INDEXES,
            headers=headers,
            auth=(PROXY_USER, PROXY_PASSWORD)
        )
    except requests.exceptions.ConnectionError as e:
        print 'ERROR: Not able to connect to URL'
        return []
    except requests.exceptions.Timeout as e:
        print 'ERROR: ElasticSearch time out'
        return []
    except requests.exceptions.HTTPError as e:
        print 'ERROR: HTTP error'
        return []
    else:
        print 'INFO: ElasticSearch response status code was %s' % req.status_code
        if req.status_code != 200:
            return []

    indexes = []

    for idx in req.json()['indices']:
        if idx.startswith(INDEX_PREFIX):
            indexes.append(idx)

    print 'INFO: Found %d indexes' % len(indexes)
    print 'INFO: Sorting indexes'
    indexes.sort()

    return indexes

# #############################################################################

def clean_index(idx):
    url = '%s/%s' % (PROXY_URL_DELETE_INDEXES, idx)

    print 'INFO: Cleaning index %s' % idx
    print 'INFO: URL=%s' % url

    headers = {
        'Content-Type': 'application/json',
    }

    try:
        req = requests.delete(
            url,
            headers=headers,
            auth=(PROXY_USER, PROXY_PASSWORD)
        )
    except requests.exceptions.ConnectionError as e:
        print 'ERROR: Not able to connect to URL'
        return 0
    except requests.exceptions.Timeout as e:
        print 'ERROR: ElasticSearch time out'
        return 0
    except requests.exceptions.HTTPError as e:
        print 'ERROR: HTTP error'
        return 0
    else:
        print 'INFO: ElasticSearch response status code was %s' % req.status_code

        if req.status_code != 200:
            return 0
        else:
            return 1

# #############################################################################

def clean_indexes():
    headers = {
        'Content-Type': 'application/json',
    }

    current_storage =  get_storage_usage()

    print 'INFO: Current storage is %s%%' % current_storage

    if current_storage == -1:
        print 'WARN: It was not able to retrieve current storage size'
    elif current_storage >= STORAGE_USAGE_MIN_THRESHOLD:
        print 'INFO: Current storage is above the threshold'

        indexes = get_indexes()

        p = re.compile('%s(\d\d\d\d)\.(\d\d)\.\d\d' % INDEX_PREFIX)
        m = p.search(indexes[0])
        year = m.group(1)
        month = m.group(2)

        print 'INFO: Script will clean indexes for %s/%s' % (year, month)

        count = 0
        total = 0

        for idx in indexes:
            if idx.startswith('%s%s.%s' % (INDEX_PREFIX, year, month)):
                total += 1
                count += clean_index(idx)

        print "INFO: %d out of %d indexes were removed" % (count, total)
    else:
        print 'INFO: Storage is fine, no index will be deleted'

# #############################################################################

def main():
    print 'INFO: Starting task'
    clean_indexes()
    print 'INFO: Completed'

# #############################################################################

if __name__ == "__main__":
    main()
