#!/bin/python3
#!usr/bin/python3

import datetime
import requests
import json
import os
from elasticsearch import Elasticsearch

es = Elasticsearch(
    ['elasticX01'],
    scheme="http",
    port=9200,
    http_auth=('elastic', 'elastic'),
    timeout=60
)

health = es.cluster.health()
print(health)


# get the names of the indexes
all_indices = es.indices.get_alias().keys()
print ("\nAttempting to delete", len(all_indices), "indexes.")

# iterate the list of indexes
#for _index in all_indices:
#    # attempt to delete ALL indices in a 'try' and 'catch block
#    try:
#        if "." not in _index: # avoid deleting indexes like `.kibana`
#            elastic.indices.delete(index=_index)
#            print ("Successfully deleted:", _index)
#    except Exception as error:
#        print ('indices.delete error:', error, 'for index:', _index)

# now create a new index
#elastic.indices.create(index="new-index")

# verify the new index was created
#final_indices = elastic.indices.get_alias().keys()
#print ("\nNew total:", len(final_indices), "indexes.")
#for _index in final_indices:
#    print ("Index name:", _index)
