After you dump the data to elastic, you will see that your disk space starts to fill up and your kibana interface slows down.

If you are deleting the indexes manually using Elastic-api, you may accidentally delete the current indexes. This is an operational error. 
To avoid this situation; there are 3 options for automatic index deletion.

1. Curator
2. ILM Policy
3. Pyscript


- Curator is a tool that automatically performs such operations according to certain rules. With Curator, you can define which index to clean and how many daily data to keep with a config-file, and it will run the background-job at certain intervals and perform the deletion process according to the rules like cronjob


[curator_Elastic](https://www.elastic.co/guide/en/elasticsearch/client/curator/5.8/ex_delete_indices.html)

```
$ sudo apt-get install python-pip

$ sudo pip install elasticsearch-curator

mkdir curator
cd curator

# curator --help
Usage: curator [OPTIONS] ACTION_FILE

  Curator for Elasticsearch indices.

  See http://elastic.co/guide/en/elasticsearch/client/curator/current

Options:
  --config PATH  Path to configuration file. Default: ~/.curator/curator.yml
  --dry-run      Do not perform any changes.
  --version      Show the version and exit.
  --help         Show this message and exit.


touch config.yml

vi config.yml

client:
    hosts:
        - 127.0.0.1
### not running connection pool / timeout
### fixed : => hosts: ["10.30.30.10"]
###  hosts:
###    - https://securehost.com:9200
    port: 9200
    url_prefix:
    use_ssl: False
    certificate:
    client_cert:
    client_key:
    ssl_no_validate: False
    http_auth:
### not running htp 
    timeout: 30
    master_only: False
 
logging:
    loglevel: INFO
    logfile:
    logformat: default
    blacklist: ['elasticsearch', 'watches', '.watches','.kibana']


##-- ex: payment-logs-* external logs;
vi actions.yml
##-- rule & actions
actions:
  1:
    action: delete_indices
    description: >-
      Delete indices older than 90 days except payment-logs indices !
    options:
      ignore_empty_list: True
      disable_action: False
    filters:
    - filtertype: pattern
      kind: prefix
      value: payment-logs-
      exclude: True
    - filtertype: age
      source: name
      direction: older
      timestring: '%Y.%m.%d'
      unit: days
      unit_count: 90


##--test;

curator --config /root/curator/config.yml --dry-run /root/curator/actions.yml

!! actions.yml == delete_indeces.yml

curator --config config.yml delete_indices.yml

crontab -e

##--every 02.00 am
0 2 * * * curator --config /root/curator/config.yml /root/curator/actions.yml

* * * * * /usr/local/bin/curator --config {config.yml location} {delete_indices.yml location} >/dev/null 2>&1

## so you can auto-delete old indexes, free up disk space and increase performance.
```
[ex](http://www.canertosuner.com/post/curator-kullanarak-elasticsearch-index-lerini-periyodik-olarak-temizleme)

[ex](https://community.talend.com/s/article/Deleting-old-indices-in-Elasticsearch-using-Curator-8Eo3d?language=en_US)


Elasticsearch Curator: Empty list

[discuss_Elastic](https://discuss.elastic.co/t/elasticsearch-curator-empty-list/237599/5)

[github_issue](https://github.com/elastic/curator/issues/1490)

[github_issue](https://github.com/elastic/curator/issues/785)


```
Problems

options:
    allow_ilm_indices: true


```

```
action_file_yml: |-
    ---
    actions:
      1:
        action: delete_indices
        description: "Clean up ES by deleting old indices"
        options:
          timeout_override:
          continue_if_exception: False
          disable_action: False
          allow_ilm_indices: true
          ignore_empty_list: True
        filters:
        - filtertype: age
          source: name
          direction: older
          timestring: '%Y.%m.%d'
          unit: days
          unit_count: 30
          field:
          stats_result:
          epoch:
          exclude: False

or

actions:
  1:
    action: delete_indices
    description: >-
      Delete indices with age greater than 4 days(based on index name), for testapp*
      prefixed indices.
    options:
      ignore_empty_list: True
      disable_action: False
      allow_ilm_indices: True
    filters:
    - filtertype: pattern
      kind: prefix
      value: testapp-
      value: metrics-
    - filtertype: age
      source: name
      direction: older
      timestring: '%Y.%m.%d'
      unit: days
      unit_count: 3

or 
### Multiple index deleted;


actions:
  1:
    action: delete_indices
    description: >-
      Delete indices with age greater than 4 days(based on index name), for dbvitapp*
      prefixed indices.
    options:
      timeout_override:
      continue_if_exception: False
      ignore_empty_list: True
      disable_action: False
      allow_ilm_indices: True
    filters:
    - filtertype: pattern
      kind: regex
      value: '^(abcapp|testaapp)-.*$'
    - filtertype: age
      source: name
      direction: older
      timestring: '%Y.%m.%d'
      unit: days
      unit_count: 3

2022-06-08 15:49:10,734 INFO      ---deleting index f5-2022.02
2022-06-08 15:49:10,734 INFO      ---deleting index f5-2022.03
2022-06-08 15:49:10,734 INFO      ---deleting index f5-2022.01
2022-06-08 15:49:10,735 INFO      ---deleting index oraclemetricbeat-2022.03
2022-06-08 15:49:10,735 INFO      ---deleting index redis-2021.11
2022-06-08 15:49:10,735 INFO      ---deleting index redis-2021.12
2022-06-08 15:49:10,735 INFO      ---deleting index vpnw-2021.10
2022-06-08 15:49:10,735 INFO      ---deleting index oraclemetricbeat-2022.02
2022-06-08 15:49:10,735 INFO      ---deleting index oraclemetricbeat-2021.12
2022-06-08 15:49:10,735 INFO      ---deleting index oraclemetricbeat-2021.11
2022-06-08 15:49:10,735 INFO      ---deleting index oraclemetricbeat-2021.10
2022-06-08 15:49:10,735 INFO      ---deleting index redis-2022.03
2022-06-08 15:49:10,735 INFO      ---deleting index redis-2022.02
2022-06-08 15:49:10,735 INFO      ---deleting index redis-2022.01
2022-06-08 15:49:10,735 INFO      ---deleting index metricbeat-2022.03
2022-06-08 15:49:10,735 INFO      ---deleting index oraclemetricbeat-2022.01

config_yml: |-
   ---
   client:
     hosts:
       - elasticsearch-master
     port: 9200
     url_prefix:
     use_ssl: True
     certificate: '/certs/elastic-stack-ca.pem'
     client_cert: '/certs/elastic-certificate.pem'
     ssl_no_validate: True
     http_auth: "XXXXX:XXXXXXXX"
     timeout: 30
     master_only: False
   logging:
     loglevel: DEBUG , INFO
     logfile:
### logfile: /var/log/curator/curator.log
     logformat: default
     blacklist: ['elasticsearch', 'urllib3']
###  blacklist: ['elasticsearch', 'watches', '.watches','.kibana','urllib3','.apm','.monitoring','.task','kibana','security','.async']

Unable to read/parse YAML file: delete_indices.yml
mapping values are not allowed here
  in "<string>", line 15, column 12:
           kind: regex

## Thx validator '!

http://www.yamllint.com/

```

```
Test
today : 09.01.2022

PUT /testapp-2022.01.01

curator --config config.yml delete_indices.yml

### Output 
2022-01-09 02:41:30,226 INFO      Trying Action ID: 1, "delete_indices": Delete indices with age greater than 4 days(based on index name), for dbvitapp* prefixed indices.
2022-01-09 02:41:30,310 INFO      Deleting 3 selected indices: [u'testaapp-2022.01.02', u'dbvitapp-2022.01.02', u'dbvitapp-2022.01.01']
2022-01-09 02:41:30,310 INFO      ---deleting index testaaapp-2022.01.02
2022-01-09 02:41:30,310 INFO      ---deleting index abcapp-2022.01.02
2022-01-09 02:41:30,310 INFO      ---deleting index abcapp-2022.01.01
2022-01-09 02:41:30,521 INFO      Action ID: 1, "delete_indices" completed.
2022-01-09 02:41:30,522 INFO      Job completed.

crontab -e 

00 6 * * * root curator /path/curator-action.yml --config /path/curator-config.yml

```




urllib3 library python

```
python3
Type "help", "copyright", "credits" or "license" for more information.
>>> import urllib3
>>> http = urllib3.PoolManager(ssl_
KeyboardInterrupt
>>> print(urllib3.__version__)
1.25.7

>>>
KeyboardInterrupt

ctrl +D

test 
>>> import urllib3
>>> http = urllib3.PoolManager()
>>> r = http.request('GET', 'http://10.30.30.10:9200')
>>> r.status
200


curl -X GET http://10.30.30.10:9200


$ python
Python 3.6.8 (v3.6.8:3c6b436a57, Dec 24 2018, 02:04:31)
[GCC 4.2.1 Compatible Apple LLVM 6.0 (clang-600.0.57)] on darwin
Type "help", "copyright", "credits" or "license" for more information.
>>> import elasticsearch
>>> c = elasticsearch.Elasticsearch()
>>> c.info()
{'name': 'myhost.local', 'cluster_name': 'foo', 'cluster_uuid': 'wGyxgqg8RU6n8Mq_d27AAQ', 'version': {'number': '7.2.0', 'build_flavor': 'default', 'build_type': 'tar', 'build_hash': '508c38a', 'build_date': '2019-06-20T15:54:18.811730Z', 'build_snapshot': False, 'lucene_version': '8.0.0', 'minimum_wire_compatibility_version': '6.8.0', 'minimum_index_compatibility_version': '6.0.0-beta1'}, 'tagline': 'You Know, for Search'}

$ python
Python 3.6.8 (v3.6.8:3c6b436a57, Dec 24 2018, 02:04:31)
[GCC 4.2.1 Compatible Apple LLVM 6.0 (clang-600.0.57)] on darwin
Type "help", "copyright", "credits" or "license" for more information.
>>> import curator
>>> c = curator.utils.get_client(hosts=["localhost"], port=9200)
>>> c.info()
{'name': 'myhost.local', 'cluster_name': 'foo', 'cluster_uuid': 'wGyxgqg8RU6n8Mq_d27AAQ', 'version': {'number': '7.2.0', 'build_flavor': 'default', 'build_type': 'tar', 'build_hash': '508c38a', 'build_date': '2019-06-20T15:54:18.811730Z', 'build_snapshot': False, 'lucene_version': '8.0.0', 'minimum_wire_compatibility_version': '6.8.0', 'minimum_index_compatibility_version': '6.0.0-beta1'}, 'tagline': 'You Know, for Search'}

```



- ILM Policy

[Index Lifecycle Management](https://www.elastic.co/guide/en/elasticsearch/reference/current/index-lifecycle-management.html)

After creating the Ilm policy as below, you need to add it with "add policy to index template" from the action section for the relevant index.

```
policy name -> deletePolicy

Create policy then enter parameters (time - size etc.) .

After scaling the delete phase phase according to index creation, the policy is created.

Select the index pattern you wish to add, and the policy should take effect immediately, and your old indices in the pattern will be deleted.
When a new index is created (for example, you have entered a new week), a policy can be created for the last 4 weeks. Thus, your index number remains constant and up-to-date.
```

[Detail](https://www.cloudsavvyit.com/7152/how-to-rotate-and-delete-old-elasticsearch-records-after-a-month/)


![ilm_policy](ss/ilm_ss.PNG)


![ilm_policy2](ss/ilm_ss2.PNG)

![ilm_policy3](ss/ilm_ss3.PNG)


![ilm_policy_add](ss/ilm_querry.PNG)

```
PUT _ilm/policy/testDeleteIndex
{
  "policy": {
    "phases": {
      "hot": {
        "min_age": "0ms",
        "actions": {
          "rollover": {
            "max_primary_shard_size": "50tb",
            "max_age": "300d"
          },
          "set_priority": {
            "priority": 100
          }
        }
      },
      "delete": {
        "min_age": "5d",
        "actions": {
          "delete": {
            "delete_searchable_snapshot": true
          }
        }
      }
    }
  }
}
```


add_policy lifecycle policy +++


![ilm_policy_add](ilm_1.PNG)

![ilm_policy](ilm.PNG)

#### Exception's

> illegal_argument_exception: setting [index.lifecycle.rollover_alias] for index [test-2022.01.08] is empty or not defined

I noticed it doesn't work without adding alias.  [stackover_Index Lifecycle policy for deleting old indices](https://stackoverflow.com/questions/63324172/index-lifecycle-policy-for-deleting-old-indices)


- Pyscript

> note:

```
agent_name = 'James Bond'
kill_count = 9

# old ways
print('{0} has killed {1} enemies '.format(agent_name,kill_count))

# f-strings way
print(f'{agent_name} has killed {kill_count} enemies')



print(f'datetimeToday: {dt} weak: {w} year: {year}')
SyntaxError: invalid syntax

I think you have an old version of python. try upgrading to the latest version of python. F-string literals have been added to python since python 3.6. you can check more about it here

#yum install python3

#python3 -V (version)
Python 3.6.8

# pip --version
pip 8.1.2 from /usr/lib/python2.7/site-packages (python 2.7)
# pip3 --version
pip 9.0.3 from /usr/lib/python3.6/site-packages (python 3.6)




    import requests
ImportError: No module named requests

ping 8.8.8.8 *>DNS OK
ping google.com *>Access to ethernet OK

yum install python3-requests -y 

!!! Installed !!!

Use $ pip install requests (or pip3 install requests for python3) if you have pip installed. If pip is installed but not in your path you can use python -m pip install requests (or python3 -m pip install requests for python3)

Alternatively you can also use sudo easy_install -U requests if you have easy_install installed.

Alternatively you can use your systems package manager:

For centos: yum install python-requests For Ubuntu: apt-get install python-requests

If you manually want to add a library to a windows machine, you can download the compressed library, uncompress it, and then place it into the Lib\site-packages folder of your python path. (For example: C:\Python27\Lib\site-packages)



From Source (Universal)
For any missing library, the source is usually available at https://pypi.python.org/pypi/. You can download requests here: https://pypi.python.org/pypi/requests

On mac osx and windows, after downloading the source zip, uncompress it and from the termiminal/cmd run python setup.py install from the uncompressed dir.

```

```
#!/bin/python3
#!usr/bin/python3

import datetime
import requests
import json
import os

logfile = 'delete_api_log'

# read file
with open('delete_api.json', 'r') as myfile:
    data = myfile.read()

dict1 = json.loads(data)


def write_log(logfilename, logmessage):
    file_object = open(logfilename, 'a', encoding='utf-8')
    file_object.write(logmessage+"\n")
    file_object.close()


def stringToList(string):
    listRes = list(string.split(" "))
    return listRes


def read_json(file_path):
    with open(file_path, "r") as f:
        return json.load(f)


dt = datetime.date.today()
wk = dt.isocalendar()[1]
year = dt.isocalendar()[0]


print('Today is {0} , week is {1} and then year is {2}'.format(dt,wk,year))
print()
print(f'datetimeToday: {dt} weak: {wk} year: {year}')

url_base = str(dict1[0]["Schema"]) + str(dict1[0]["Username"]) + ":" + str(dict1[0]["Password"]) + "@" + str(dict1[0]["Server"]) + ":" + str(dict1[0]["Port"])+"/"
response_url = url_base + "_cat/indices?pretty=true"
response = requests.get(response_url)

print(dict1[0]["Schema"])

#if response.status_code == 200:
print('---------Success!')
a = stringToList(response.text)
#print(a)
new_list = []
search_list = []
i = wk-9
#print(id(i))
while i > 6:
    SEARCHTEXT = '2021.' + str(i)
    search_list.append(SEARCHTEXT)
    i -= 1

print(search_list)



for i in range(len(search_list)):
    for item in a:
        if search_list[i] in item:
            new_list.append(item)



result = [x for x in new_list if not x.startswith('.monitoring')]

print(result)


started_time = datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S.%f')
start_message = "Starting at " + str(started_time)
write_log(logfile + "-" + str(year) + ".log", start_message)

for i in range(len(result)):
    try:
        url_used = url_base + str(result[i])
        print(url_used)
        response = requests.delete(url_used, timeout=5)
#response = requests.get(url_used, timeout=5)
        elps_time = response.elapsed
        message = str(url_used)+" " + str(elps_time)
    except:
        message = str(url_used) + "Bağlanılamadı"

    write_log(logfile + "-" + str(year) + ".log", message)

finished_time = datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S.%f')
write_log(logfile + "-" + str(year) + ".log", "Finished at " + str(finished_time) + '\n\n')

```

delete_api.json


```
[
 {
   "Schema": "http://",
   "Server": "ElasticX01",
   "Port": "9200",
   "Username": "elastic",
   "Password": "elastic"
 }
]

PUT /iis-2021.33
PUT /iis-2021.35

./autoDeleteIndex.py
2022-01-01
Today is 2022-01-01 , week is 52 and then year is 2021

datetimeToday: 2022-01-01 weak: 52 year: 2021
http://
http://elastic:elastic@ElasticX01:9200/_cat/indices/iis-*?pretty=true
<Response [200]>
---------Success!
<class 'list'>
result is: ['iis-2021.35', 'iis-2021.33']
<class 'list'>
http://elastic:elastic@ElasticX01:9200/iis-2021.35
http://elastic:elastic@ElasticX01:9200/iis-2021.33


# cat delete_api_log-2021.log

Starting at 2022-01-01 02:22:16.823211
http://elastic:elastic@ElasticX01:9200/iis-2021.35 0:00:00.781968
http://elastic:elastic@ElasticX01:9200/iis-2021.33 0:00:00.118735
Finished at 2022-01-01 02:22:17.729662



--OTHERS--

PUT _template/custom_monitoring_template
{
  "index_patterns": [
    ".monitoring-es-*"
  ],
  "mappings": {
    "properties": {
      "index_stats": {
        "properties": {
          "shards": {
            "properties": {
              "total": {
                "type": "long"
              }
            }
          }
        }
      }
    }
  }
}


GET /_aliases?pretty=true

GET /_cat/indices?v

GET /_cat/indices?h=index

GET /_cat/indices?pretty=true


GET _cat/indices?v&s=index

GET /_search


GET _async_search



```




- Manuel ( Delete index api)

```
$ curl 'localhost:9200/_cat/indices?v'
health index pri rep docs.count docs.deleted bookstore.size pri.bookstore.size

#list all index 
curl -XGET http://localhost:9200/_cat/indices?v 

#delete index:         
curl -XDELETE 'localhost:9200/index_name'

#delete all indices:   
curl -XDELETE 'localhost:9200/_all'

#delete document   :   
curl -XDELETE 'localhost:9200/index_name/type_name/document_id'

$ curl -XDELETE 'localhost:9200/index/type/document'
ex:
$ curl -XDELETE 'localhost:9200/bookstore/book/1'

DELETE /my-index-000001

####################################################################
ex:

from elasticsearch import Elasticsearch

es = Elasticsearch([{'host':'localhost', 'port':'9200'}])

es.index(index='grades',doc_type='ist_samester',id=1,body={
    "Name":"Programming Fundamentals",
    "Grade":"A"
})

es.indices.delete(index='grades')

####################################################################

delete index.python

from elasticsearch import Elasticsearch
es = Elasticsearch(
    ['host1'],
    scheme="http",
    port=9200,
    http_auth=('elastic', 'elastic'),
    timeout=30
)

index = "my_index*"
es.indices.delete(index = index)


####################################################################
#delete all index with python

import requests
import json

ES_HOST = "http://localhost:9200"
ES_URL = f"{ES_HOST}/_cat/indices?format=json"

indexes = requests.get(ES_URL).content
indexes = json.loads(index.decode())

for i in index:
    index_name = i['index']
    content = requests.delete(f"{ES_HOST}/{index_name}").content
    print(content)
####################################################################



GET /_cat/indices?v

```
[ibmIndex](https://www.ibm.com/docs/en/cloud-private/3.1.2?topic=logging-manually-removing-log-indices)

[IndexHowTo](https://kb.objectrocket.com/elasticsearch/how-to-delete-an-index-in-elasticsearch-using-kibana)
