# ELK

#### Neden Elastic?

- [ ] Application search
- [ ] Enterprise search
- [ ] Performance monitoring
- [ ] Data Analytics
- [ ] security, business analytics etc.



# Beats - Transfer

[Beats](https://www.elastic.co/beats) go ile yazılmış, dataları toplayan ve taşıyan agentlardır.


Beat  | Tanım
--- | ---
[Auditbeat](https://github.com/elastic/beats/tree/master/auditbeat) | Audit datalarını izler.
[Filebeat](https://github.com/elastic/beats/tree/master/filebeat) | Log dosyaları ve tail -f
[Functionbeat](https://github.com/elastic/beats/tree/master/x-pack/functionbeat) | Bulut sağlayıcı eventlerini okur ve gönderir (serverless infrastructure,Cloud data)
[Heartbeat](https://github.com/elastic/beats/tree/master/heartbeat) | Uzak sunuculara ping atar (availability,Availability monitoring)
[Journalbeat](https://github.com/elastic/beats/tree/master/journalbeat) | Journald eventlerini okur ve gönderir,Systemd journals
[Metricbeat](https://github.com/elastic/beats/tree/master/metricbeat) | İşletim sistemi ve servislerin metriklerini dinler.
[Packetbeat](https://github.com/elastic/beats/tree/master/packetbeat) | network ve uygulamaların paketlerini dinler.
[Winlogbeat](https://github.com/elastic/beats/tree/master/winlogbeat) | Windows eventlerini dinler.
[Osquerybeat](https://github.com/elastic/beats/tree/master/x-pack/osquerybeat) | Osquery çalıştırır ve etkileşimi yönetir.


## Dokümanlar ve Moduller


* [Beats platform](https://www.elastic.co/guide/en/beats/libbeat/current/index.html)
* [Auditbeat](https://www.elastic.co/guide/en/beats/auditbeat/current/index.html)
* [Filebeat](https://www.elastic.co/guide/en/beats/filebeat/current/index.html)
* [Functionbeat](https://www.elastic.co/guide/en/beats/functionbeat/current/index.html)
* [Heartbeat](https://www.elastic.co/guide/en/beats/heartbeat/current/index.html)
* [Journalbeat](https://www.elastic.co/guide/en/beats/journalbeat/current/index.html)
* [Metricbeat](https://www.elastic.co/guide/en/beats/metricbeat/current/index.html)
* [Packetbeat](https://www.elastic.co/guide/en/beats/packetbeat/current/index.html)
* [Winlogbeat](https://www.elastic.co/guide/en/beats/winlogbeat/current/index.html)



metricbeat modules enable apache mysql

metricbeat modules list

![ELASTIC](Elastic_sql.PNG)


Kibanada hazır dash. bulunur, dolayısıyla dogrudan ilgili sunucuya gönderilirse dash. dolar.

```
metricbeat.yml

metricbeat.config.modules:
  path: ${path.config}/modules.d/*.yml

  reload.enabled: false

setup.template.settings:
  index.number_of_shards: 1
  index.codec: best_compression

setup.dashboards.enabled: true

setup.kibana:

  host: "<hostname>:5601"

output.elasticsearch:
  hosts: ["<hostname>:9200"]

processors:
  - add_host_metadata: ~
```

> systemctl status metricbeat

[metricbeat index](https://www.elastic.co/guide/en/beats/metricbeat/current/change-index-name.html)

```
metricbeat setup --dashboards

Loading dashboards (Kibana must be running and reachable)
Exiting: Kibana version must be at least 7.14.0

cd /usr/share/kibana/
bin/kibana --version

Kibana should not be run as root.  Use --allow-root to continue.

# bin/kibana --version --allow-root
7.9.2

Must be upgrade kibana version

```
or 

```
metricbeat modules enable kafka
Enabled kafka

metricbeat modules disable kafka
Disabled kafka


metricbeat setup

Overwriting ILM policy is disabled. Set `setup.ilm.overwrite: true` for enabling.

Index setup finished.
Loading dashboards (Kibana must be running and reachable)
Exiting: Kibana version must be at least 7.14.0

vi metricbeat.reference.yml

/setup.ilm.overwrite: true

metricbeat -e

```


Upgrade Elasitc


```

!!!!!!!!!  must be snapshot for elastic index and servers !!!!!!!!!

wget https://artifacts.elastic.co/downloads/elasticsearch/elasticsearch-7.15.2-x86_64.rpm

scp elastic-7.15.2-x86_64.rpm root@<hostname>:/tmp

systemctl stop elasticsearch.service

systemctl daemon-reload

tmp]# rpm -Uvh elasticsearch-7.15.2-x86_64.rpm

systemctl start elasticsearch.service


!!! all elastic cluster must be upgraded.

```


Upgrade Kibana

> 7.0–7.13 -> Upgrade to 7.15.0


```
systemctl status kibana
● kibana.service - Kibana
   Loaded: loaded (/etc/systemd/system/kibana.service; enabled; vendor preset: disabled)
   Active: active (running) since was born :)

wget https://artifacts.elastic.co/downloads/kibana/kibana-7.15.2-x86_64.rpm

scp kibana-7.15.0-x86_64.rpm root@<hostname>:/tmp -y

rpm -qa | grep kibana

yum update -y

_install rpm new packages_

systemctl stop kibana

systemctl status kibana

yum update kibana -y

systemctl daemon-reload

systemctl start kibana

systemctl status kibana


rpm -ivh kibana-7.15.0-x86_64.rpm

rpm -Uvh kibana-7.15.0-x86_64.rpm

Preparing...                          ################################# [100%]
Updating / installing...
   1:kibana-7.15.0-1                  warning: /etc/kibana/kibana.yml created as /etc/kibana/kibana.yml.rpmnew
################################# [ 50%]
Stopping kibana service...Stopping kibana (via systemctl): [  OK  ]
 OK
Cleaning up / removing...
   2:kibana-7.9.2-1                   ################################# [100%]


cd /usr/share/kibana/
bin/kibana --version --allow-root 

7.15.0

curl -XGET http://<hostname>:5601/status -I

curl -XGET http://<hostname>:5601

Kibana server is not ready yet[

-->  running after elastic upgrade http 200 ok

lsof -i -P -n | grep 5601


journalctl --unit kibana -r

-r => reverse

curl -XGET http://<hostname>:5601/status -I
HTTP/1.1 200 OK

Now metricbeat setup quickly

metricbeat -e

INFO    [monitoring]    log/log.go:142  Starting metrics logging every 30s

metricbeat setup --dashboards


```

> http://<localhost>:5601/app/home#/

load a data set and a kibana dashboard

```
sudo metricbeat modules enable elasticsearch

sudo metricbeat setup
sudo service metricbeat start

Overwriting ILM policy is disabled. Set `setup.ilm.overwrite: true` for enabling.

Index setup finished.
Loading dashboards (Kibana must be running and reachable)

Loaded dashboards


```

[kiba7.15](https://www.elastic.co/guide/en/kibana/7.15/dashboard.html)


[kube](https://medium.com/nerd-for-tech/monitoring-kubernetes-cluster-using-elastic-stack-elasticsearch-kibana-metricbeat-ecbe586a3e7c)



[upgrade video](https://www.youtube.com/watch?v=ah3H-V16qgU&t=7s)




```
localhost:5601/status
Kibana status is Green


systemctl status metricbeat -l

```



## Elastic Query

### Cluster Node Info

GET /_nodes/<metric>
GET /_nodes/<node_id>/<metric>

### Process

GET /_nodes/process

`curl -X GET "localhost:9200/_nodes/process?pretty"`

>equ

GET /_nodes/_all/process

`curl -X GET "localhost:9200/_nodes/_all/process?pretty"`

### nodeId1 & nodeId2 process jvm

GET /_nodes/nodeId1,nodeId2/jvm,process

`curl -X GET "localhost:9200/_nodes/nodeId1,nodeId2/jvm,process?pretty"`

>equ

GET /_nodes/nodeId1,nodeId2/info/jvm,process

`curl -X GET "localhost:9200/_nodes/nodeId1,nodeId2/info/jvm,process?pretty"`

### Sadece nodeId1 & nodeId2 tüm bilgileri

GET /_nodes/nodeId1,nodeId2/_all

`curl -X GET "localhost:9200/_nodes/nodeId1,nodeId2/_all?pretty"`

### Plugins

GET /_nodes/plugins

`curl -X GET "localhost:9200/_nodes/plugins?pretty"`

### Ingest

GET /_nodes/ingest

`curl -X GET "localhost:9200/_nodes/ingest?pretty"`

### Hot thread

GET /_nodes/hot_threads

GET /_nodes/nodeId1,nodeId2/hot_threads

```
curl -X GET "localhost:9200/_nodes/hot_threads?pretty"
curl -X GET "localhost:9200/_nodes/nodeId1,nodeId2/hot_threads?pretty"
```

### Node Info

#### Tüm nodelar
GET /_nodes
>equ

GET /_nodes/_all

#### local node
GET /_nodes/_local
#### master node
GET /_nodes/_master
#### Belirli bir node
GET /_nodes/node_name_goes_here
GET /_nodes/node_name_goes_*
#### ıp addr
GET /_nodes/10.0.0.3,10.0.0.4
GET /_nodes/10.0.0.*
#### Role göre
GET /_nodes/_all,master:false
GET /_nodes/data:true,ingest:true
GET /_nodes/coordinating_only:true
GET /_nodes/master:true,voting_only:false
#### Özelliğe göre
GET /_nodes/rack:2
GET /_nodes/ra*:2
GET /_nodes/ra*:2*


### Cluster settings Api

>Shard akışı için açıklama

GET _cluster/allocation/explain
{
  "index": "my-index-000001",
  "shard": 0,
  "primary": false,
  "current_node": "my-node"
}

```
curl -X GET "localhost:9200/_cluster/allocation/explain?pretty" -H 'Content-Type: application/json' -d'
{
  "index": "my-index-000001",
  "shard": 0,
  "primary": false,
  "current_node": "my-node"
}
'
```
> GET _cluster/allocation/explain
> POST _cluster/allocation/explain


>Adreslenmemiş primary shards

GET _cluster/allocation/explain
{
  "index": "my-index-000001",
  "shard": 0,
  "primary": true
}

```
curl -X GET "localhost:9200/_cluster/allocation/explain?pretty" -H 'Content-Type: application/json' -d'
{
  "index": "my-index-000001",
  "shard": 0,
  "primary": true
}
'
```
GET _cluster/allocation/explain

`curl -X GET "localhost:9200/_cluster/allocation/explain?pretty"`


### Cluster health Api

> health status -> green, yellow, red

GET /_cluster/health?wait_for_status=yellow&timeout=50s

`curl -X GET "localhost:9200/_cluster/health?wait_for_status=yellow&timeout=50s&pretty"`

GET _cluster/health

`curl -X GET "localhost:9200/_cluster/health?pretty"`

>shard levelde cluster health

GET /_cluster/health/my-index-000001?level=shards

`curl -X GET "localhost:9200/_cluster/health/my-index-000001?level=shards&pretty"`


### Cluster reroute Api

>Shardların paylaşımlarını degiştirir.

POST /_cluster/reroute

_ex_

POST /_cluster/reroute
{
  "commands": [
    {
      "move": {
        "index": "test", "shard": 0,
        "from_node": "node1", "to_node": "node2"
      }
    },
    {
      "allocate_replica": {
        "index": "test", "shard": 1,
        "node": "node3"
      }
    }
  ]
}


```
curl -X POST "localhost:9200/_cluster/reroute?pretty" -H 'Content-Type: application/json' -d'
{
  "commands": [
    {
      "move": {
        "index": "test", "shard": 0,
        "from_node": "node1", "to_node": "node2"
      }
    },
    {
      "allocate_replica": {
        "index": "test", "shard": 1,
        "node": "node3"
      }
    }
  ]
}
'
```

### Cluster State Api

>Cluster metadataları hkk.

GET /_cluster/state/<metrics>/<target>

GET /_cluster/state/_all/foo,bar

`curl -X GET "localhost:9200/_cluster/state/_all/foo,bar?pretty"`

GET /_cluster/state/blocks

`curl -X GET "localhost:9200/_cluster/state/blocks?pretty"`


### Cluster Stats Api

GET /_cluster/stats?human&pretty

`curl -X GET "localhost:9200/_cluster/stats?human&pretty&pretty"`

>Alt küme sınıflandırma
GET /_cluster/stats/nodes/node1,node*,master:false

`curl -X GET "localhost:9200/_cluster/stats/nodes/node1,node*,master:false?pretty"`

[Cluster Stats Detay](https://www.elastic.co/guide/en/elasticsearch/reference/current/cluster-stats.html)

### Cluster Storage ayarı

PUT /_cluster/settings
{
  "persistent" : {
    "indices.recovery.max_bytes_per_sec" : "50mb"
  }
}

```
curl -X PUT "localhost:9200/_cluster/settings?pretty" -H 'Content-Type: application/json' -d'
{
  "persistent" : {
    "indices.recovery.max_bytes_per_sec" : "50mb"
  }
}
'
```
>Gecici güncelleme

PUT /_cluster/settings?flat_settings=true
{
  "transient" : {
    "indices.recovery.max_bytes_per_sec" : "20mb"
  }
}

```
curl -X PUT "localhost:9200/_cluster/settings?flat_settings=true&pretty" -H 'Content-Type: application/json' -d'
{
  "transient" : {
    "indices.recovery.max_bytes_per_sec" : "20mb"
  }
}
'
```


### Cluster Ayarlarını resetler

PUT /_cluster/settings
{
  "transient" : {
    "indices.recovery.max_bytes_per_sec" : null
  }
}

```
curl -X PUT "localhost:9200/_cluster/settings?pretty" -H 'Content-Type: application/json' -d'
{
  "transient" : {
    "indices.recovery.max_bytes_per_sec" : null
  }
}
'
```
> indices.recovery ayarlarını resetlemek için

PUT /_cluster/settings
{
  "transient" : {
    "indices.recovery.*" : null
  }
}

```
curl -X PUT "localhost:9200/_cluster/settings?pretty" -H 'Content-Type: application/json' -d'
{
  "transient" : {
    "indices.recovery.*" : null
  }
}
'
```
# Genel

#### Indexleri getir

GET /_nodes/stats/indices

#### İşletim sistemi ve processler
GET /_nodes/stats/os,process

#### ıp addr. için processler
GET /_nodes/10.0.0.1/stats/process

## Node, Index, shards

#### Node tarafından Fielddata özet
GET /_nodes/stats/indices/fielddata?fields=field1,field2

#### Node ve index tarafından Fielddata özet
GET /_nodes/stats/indices/fielddata?level=indices&fields=field1,field2

#### Node, index, shards tarafından Fielddata özet
GET /_nodes/stats/indices/fielddata?level=shards&fields=field1,field2

#### Alan adları
GET /_nodes/stats/indices/fielddata?fields=field*

## Arama grupları

#### Tüm grup ve stats
GET /_nodes/stats?groups=_all

#### Seçili group bazlı sadece index istatistikleri
GET /_nodes/stats/indices?groups=foo,bar

#### ingest-related node istatistikleri
GET /_nodes/stats?metric=ingest&filter_path=nodes.*.ingest.pipelines

## Pending statudeki cluster info
GET /_cluster/pending_tasks

## Remote statudeki cluster info

GET /_remote/info

## Yürütülmekte olan tasklar
GET /_tasks


> ex

```
GET _tasks 
GET _tasks?nodes=nodeId1,nodeId2 
GET _tasks?nodes=nodeId1,nodeId2&actions=cluster:*


GET _tasks/oTUltX4IQMOUUVeiohTt8A:124
GET _tasks?parent_task_id=oTUltX4IQMOUUVeiohTt8A:123

>Task hkk. detay getirir fakat risklidir.
GET _tasks?actions=*search&detailed
```

> Uzun süren görevleri iptal etmek için

`POST _tasks/oTUltX4IQMOUUVeiohTt8A:12345/_cancel`

> Birden cok task iptali için ex;
`POST _tasks/_cancel?nodes=nodeId1,nodeId2&actions=*reindex`

>Gruplandırmayı üst görevlere göre degiştirecektir.
GET _tasks?group_by=parents

>disabled
GET _tasks?group_by=none

>votingden node1 ve node2yi cıkarır
POST /_cluster/voting_config_exclusions?node_names=nodeName1,nodeName2

>Listeden tüm istisnalar kaldırılır.
DELETE /_cluster/voting_config_exclusions




[INDEX hkk detay](https://www.elastic.co/guide/en/elasticsearch/reference/current/indices.html)

Index; veri tabanındaki tablolar gibidir. Datalar; elastic shardlarında depolanıyor. Clusterdaki shard ve replicalarda saklanan dokumanlara (doc) ukaşmak için indexleri kullanılır. Shardlar, kümeler içindeki farklı nodelarda olabilir. Replika ise index_shardların kopyalarıdır.

Shard ve replica

```
Resource: -> KULEKCI Haydar
Bir “index”, bir “node”a sığmayacak kadar büyük veri setini içerebilir. Örneğin bir “index” içerisinde milyarlarca döküman olabilir ve bunların büyüklükleri 1TB’a ulaşabilir ve bir “node” üzerindeki disklerinizin büyüklüğü bu kadar olmayabilir. Diğer taraftan sadece bir node üzerinden bu büyük veriyi sunmanız aramalarınızı yavaşlatabilir.

Bu problemi çözmek için, Elasticsearch “shard” adı verilen, index’inizi alt parçalara bölen, bir yapı sunmaktadır. Elasticsearch sayesinde bir “index” oluşturueken rahatça kaç “shard” olması gerektiğini belirtebilirsiniz. Her bir “shard” aslında kendi başına tam donanımlı birer “index”dir.

“Sharding” 2 sebeple önemlidir:

_Yatayda ölçeklendirme imkanı sunar
Arama Performansınızı/İşlem hacminizi artırmak için işlemlerinizi birden fazla “shard”a(ya da “node”a) dağıtır ve paralel bir şekilde çalışmasını sağlar._


“Shard”ların nasıl dağıtık olduğunu ve “shard”lara ayrılmış dökümanların arama sırasında nasıl geri birleştirildiği tamamen Elasticsearch tarafından yönetilmektedir ve kullanıcı olarak ekstra bir çaba harcamanız gerekmez.

Ağ ve bulut mimarilerinde herhangi bir zamanda hatalar olabilmektedir. Bir “node” ya da “shard” herhangi bir sebeple ulaşılamaz olduğunda ya da bozulduğunda, bu hata durumunu düzeltecek bir sistemin olması gerçekten çok yararlıdır ve aynı zamanda tavsiye edilmektedir. Bu amaçla, Elasticsearch “index shard”larının bir veya daha fazla kopyalarını oluşturmaktadır ve bunlara “replica shard” denilmektedir. İngilizce olarak kısaca “replicas” denilmektedir.

“Replication” 2 sebeple çok önemlidir:

_Shard veya node hata verdiğinde yüksek erişilebilirlik imkanı sunar. Bu sebeple, bir replica shard’ın, kendini kopyaladığı ana shard ile aynı node içerisinde olmamaması gerekmektedir.
Aramalarınız paralel olarak tüm replica’larda çalıştığı için arama hazminizi veya işlem hacminizi ölçeklendirmenize olanak sağlar._


Özetlemek gerekirse, her bir “index” bir veya daha fazla “shard”a ayrılır. Bir “index”te hiç “replica” olmayabilir veya birden fazla da olabilir. Bir kez “replica” edilirse her bir “index”in bir “primary shard”ları ve aynı zamanda “primary shard”ların kopyaları olan “replica shard”ları olacaktır. “Shard” sayıları ve “replica” sayıları “index” oluşturulurken her bir “index” için ayrı ayrı verilebilir. “index” oluşturulduktan sonra “replica shard” sayısı dinamik olarak değişitirilebilirken, “shard” sayısı değişitirilemez.

Varsayılan olarak, Elasticsearch’de her bir “index” 5 “primary shard” 1 “replica” ile oluşturulur. Bunun anlamı, eğer “cluster” içerisinde 2 “node” var ise 5 “primary shard” ve 5 “replica shard” ile birlikte toplamda her bir “index”iniz için 10 “shard” olacaktır.

Note: Her Elascticsearch shard’ı bir Lucene Index‘dir. Bu “index” için bir maximum döküman sayısı mevcuttur. LUCENE-5843‘e göre limit 2,147,483,519 (= Integer.MAX_VALUE - 128) dökümandır. Shard boyutunuzu _cat/shards isteğinde bulunarak görebilirsiniz.
```

```
$ du -b --max-depth=1 /var/lib/elasticsearch/[environment name]/nodes/0/indices \
    | sort -rn | numfmt --to=iec --suffix=B --padding=5

GET /_cat/allocation?v&pretty
```


> GET _cat/indices?v

— health: index health (green, yellow, red)
— status: index current status (open, closed)
— index: name of the index
— uuid: Index unique name. Elasticsaerch generates a unique name for each index and uses this name for its internal operations.
— pri: number of primary shards
— rep: number of replica shards
— docs.count: number of documents
— docs.deleted: number of deleted documents
— store.size: total size of all shards (primary and replicas)
— pri.store.size: total size of primary shards

GET _cat/indices?v&s=index

GET _cat/indices?s=status,pri,rep

GET _cat/indices?v&h=health,status,index,pri,rep

GET _cat/indices/index*?v&h=health,status,index,pri,rep

GET _cat/indices/test*?v&h=health,status,index,pri,rep

GET _cat/indices?v&h=health,status,index,pri,rep&format=yaml

GET _cat/indices?v&h=health,status,index,pri,rep&format=json

v parametresi -> Başlıkları görüntülemeyi sağlar.
s parametresi -> Sıralama olanağı sağlar.
h parametresi -> Sonucu filtreleme olanağı sağlar.
Bu _cat/* default olr sonucunu txt olarak döner. Json formatta almak için &format=json kullanılabilir.


Ayrıca kümedeki index listesini alias(takma ad) ile almak için;

GET _alias

Yada; 

GET _cluster/health?level=indices



[Calculator](https://gbaptista.github.io/elastic-calculator/)






```
######################################################################################

systemctl start docker

docker ps

docker ps -a

docker run -it docker.elastic.co/elasticsearch/elasticsearch:8.0.0-alpha1

docker start <container_id>

docker pull docker.elastic.co/beats/metricbeat:7.15.2

docker run \
docker.elastic.co/beats/metricbeat:7.15.2 \
setup -E setup.kibana.host=kibana:5601 \
-E output.elasticsearch.hosts=["elasticsearch:9200"] 

curl -L -O https://raw.githubusercontent.com/elastic/beats/7.15/deploy/docker/metricbeat.docker.yml

docker run -d \
  --name=metricbeat \
  --user=root \
  --volume="$(pwd)/metricbeat.docker.yml:/usr/share/metricbeat/metricbeat.yml:ro" \
  --volume="/var/run/docker.sock:/var/run/docker.sock:ro" \
  --volume="/sys/fs/cgroup:/hostfs/sys/fs/cgroup:ro" \
  --volume="/proc:/hostfs/proc:ro" \
  --volume="/:/hostfs:ro" \
  docker.elastic.co/beats/metricbeat:7.15.2 metricbeat -e \
  -E output.elasticsearch.hosts=["elasticsearch:9200"]

docker run \
  --label co.elastic.logs/module=apache2 \
  --label co.elastic.logs/fileset.stdout=access \
  --label co.elastic.logs/fileset.stderr=error \
  --label co.elastic.metrics/module=apache \
  --label co.elastic.metrics/metricsets=status \
  --label co.elastic.metrics/hosts='${data.host}:${data.port}' \
  --detach=true \
  --name my-apache-app \
  -p 8080:80 \
  httpd:2.4




curl -L -O https://artifacts.elastic.co/downloads/beats/metricbeat/metricbeat-7.15.2-linux-x86_64.tar.gz

tar xzvf metricbeat-7.15.2-linux-x86_64.tar.gz




cat metricbeat.yml

setup.kibana:

  host: "<hostname>:5601"

output.elasticsearch:
  hosts: ["<hostname>:9200"]



***************************************

metricbeat.config.modules:
  path: ${path.config}/modules.d/*.yml

  reload.enabled: false

setup.template.settings:
  index.number_of_shards: 2
  index.codec: best_compression

setup.dashboards.enabled: true

setup.kibana:

  host: "<hostname>:5601"

output.elasticsearch:
  hosts: ["<hostname>:9200"]

processors:
  - add_host_metadata: ~
  - add_cloud_metadata: ~
  - add_docker_metadata: ~
  - add_kubernetes_metadata: ~


 cat kibana.yml
# Module: kibana
# Docs: https://www.elastic.co/guide/en/beats/filebeat/master/filebeat-module-kibana.html

- module: kibana
  # Server logs
  log:
    enabled: true

    # Set custom paths for the log files. If left empty,
    # Filebeat will choose the paths depending on your OS.
    #var.paths:

  # Audit logs
  audit:
    enabled: true

    # Set custom paths for the log files. If left empty,
    # Filebeat will choose the paths depending on your OS.
    #var.paths:

./metricbeat modules list

./metricbeat modules enable apache mysql kafka

./metricbeat setup -e

Index setup finished.
Loading dashboards (Kibana must be running and reachable)
2021-11-17T14:26:39.662-0500    INFO    kibana/client.go:167    Kibana url: http://192.168.1.120:5601
2021-11-17T14:26:40.706-0500    INFO    kibana/client.go:167    Kibana url: http://192.168.1.120:5601
2021-11-17T14:28:25.177-0500    INFO    instance/beat.go:848    Kibana dashboards successfully loaded.
Loaded dashboards

```

```
./metricbeat --help
Usage:
  metricbeat [flags]
  metricbeat [command]

Available Commands:
  export      Export current config or index template
  help        Help about any command
  keystore    Manage secrets keystore
  modules     Manage configured modules
  run         Run metricbeat
  setup       Setup index template, dashboards and ML jobs
  test        Test config
  version     Show current version info

Flags:
  -E, --E setting=value              Configuration overwrite
  -N, --N                            Disable actual publishing for testing
  -c, --c string                     Configuration file, relative to path.config (default "metricbeat.yml")
      --cpuprofile string            Write cpu profile to file
  -d, --d string                     Enable certain debug selectors
  -e, --e                            Log to stderr and disable syslog/file output
      --environment environmentVar   set environment being ran in (default default)
  -h, --help                         help for metricbeat
      --httpprof string              Start pprof http server
      --memprofile string            Write memory profile to this file
      --path.config string           Configuration path
      --path.data string             Data path
      --path.home string             Home path
      --path.logs string             Logs path
      --plugin pluginList            Load additional plugins
      --strict.perms                 Strict permission checking on config files (default true)
      --system.hostfs string         Mount point of the host's filesystem for use in monitoring a host from within a container
  -v, --v                            Log at INFO level

Use "metricbeat [command] --help" for more information about a command.

```

> Filebeat multiple input session ;


```
filebeat.inputs:
- type: filestream
  paths:
    - /var/log/messages
    - /var/log/*.log



filebeat.inputs:
- type: filestream 
  paths:
    - /var/log/system.log
    - /var/log/wifi.log
- type: filestream 
  paths:
    - "/var/log/apache2/*"
  fields:
    apache: true
```

> Filebeat regular expression & include / exclude input session ;

```
filebeat.inputs:
- type: filestream
  ...
  exclude_lines: ['^DBG']

filebeat.inputs:
- type: filestream
  ...
  include_lines: ['^ERR', '^WARN']

filebeat.inputs:
- type: filestream
  ...
  include_lines: ['sometext']
  exclude_lines: ['^DBG']
```




> Parsers

```
filebeat.inputs:
- type: filestream
  ...
  parsers:
    - ndjson:
      keys_under_root: true
      message_key: msg
    - multiline:
      type: counter
      lines_count: 3
```

Common

```
filebeat.inputs:
- type: filestream
  . . .
  tags: ["json"]


filebeat.inputs:
- type: filestream
  . . .
  fields:
    app_id: query_engine_12


```

> Others

```
[esckins@test ~]$ sudo filebeat test config
Config OK
[esckins@test ~]$ sudo filebeat test output
Kafka: host1:9600...
  parse host... OK
  dns lookup... OK
  addresses: $host1
  dial up... OK
Kafka: host2:9600...
  parse host... OK
  dns lookup... OK
  addresses: $host2
  dial up... OK
Kafka: host3:9600...
  parse host... OK
  dns lookup... OK
  addresses: $host3
  dial up... OK

sudo filebeat setup





output {
  if "shouldmail" in [tags] {
    email {
      to => 'technical@example.com'
      from => 'monitor@example.com'
      subject => 'Alert - %{title}'
      body => "Tags: %{tags}\\n\\Content:\\n%{message}"
      #template_file => "/tmp/email_template.mustache"
      #body => "Tags: %{tags}\\n\\Content:\\n%{message}"
      #attachments => ["C:\picture.jpg"]
      domain => 'mail.example.com'
      #address =>"mail.relay@v.com.tr"
      port => 25
    }
  }
}

#    to => 'no@reply.com'
#    attachments => [{ 
#      filename => 'attachment_name.txt'
#      content => "%{message}"
#    }]


#if "java.lang.RuntimeException" in [message] {
#email {
#from => "##########.com"
#body => "Here is the event :%{message} \nLog file: %{path}"


    filter {
        grok {
          match => {"message" => "%{INT:count} %{GREEDYDATA:message}"
          overwrite => ["message"]
        } 
    }










examples:

input {
  beats {
    port => 5044
  }
}



filter {
  if [fileset][module] == "system" {
    if [fileset][name] == "auth" {
      grok {
        match => { "message" => ["%{SYSLOGTIMESTAMP:[system][auth][timestamp]} %{SYSLOGHOST:[system][auth][hostname]} sshd(?:\[%{POSINT:[system][auth][pid]}\])?: %{DATA:[system][auth][ssh][event]} %{DATA:[system][auth][ssh][method]} for (invalid user )?%{DATA:[system][auth][user]} from %{IPORHOST:[system][auth][ssh][ip]} port %{NUMBER:[system][auth][ssh][port]} ssh2(: %{GREEDYDATA:[system][auth][ssh][signature]})?",
                  "%{SYSLOGTIMESTAMP:[system][auth][timestamp]} %{SYSLOGHOST:[system][auth][hostname]} sshd(?:\[%{POSINT:[system][auth][pid]}\])?: %{DATA:[system][auth][ssh][event]} user %{DATA:[system][auth][user]} from %{IPORHOST:[system][auth][ssh][ip]}",
                  "%{SYSLOGTIMESTAMP:[system][auth][timestamp]} %{SYSLOGHOST:[system][auth][hostname]} sshd(?:\[%{POSINT:[system][auth][pid]}\])?: Did not receive identification string from %{IPORHOST:[system][auth][ssh][dropped_ip]}",
                  "%{SYSLOGTIMESTAMP:[system][auth][timestamp]} %{SYSLOGHOST:[system][auth][hostname]} sudo(?:\[%{POSINT:[system][auth][pid]}\])?: \s*%{DATA:[system][auth][user]} :( %{DATA:[system][auth][sudo][error]} ;)? TTY=%{DATA:[system][auth][sudo][tty]} ; PWD=%{DATA:[system][auth][sudo][pwd]} ; USER=%{DATA:[system][auth][sudo][user]} ; COMMAND=%{GREEDYDATA:[system][auth][sudo][command]}",
                  "%{SYSLOGTIMESTAMP:[system][auth][timestamp]} %{SYSLOGHOST:[system][auth][hostname]} groupadd(?:\[%{POSINT:[system][auth][pid]}\])?: new group: name=%{DATA:system.auth.groupadd.name}, GID=%{NUMBER:system.auth.groupadd.gid}",
                  "%{SYSLOGTIMESTAMP:[system][auth][timestamp]} %{SYSLOGHOST:[system][auth][hostname]} useradd(?:\[%{POSINT:[system][auth][pid]}\])?: new user: name=%{DATA:[system][auth][user][add][name]}, UID=%{NUMBER:[system][auth][user][add][uid]}, GID=%{NUMBER:[system][auth][user][add][gid]}, home=%{DATA:[system][auth][user][add][home]}, shell=%{DATA:[system][auth][user][add][shell]}$",
                  "%{SYSLOGTIMESTAMP:[system][auth][timestamp]} %{SYSLOGHOST:[system][auth][hostname]} %{DATA:[system][auth][program]}(?:\[%{POSINT:[system][auth][pid]}\])?: %{GREEDYMULTILINE:[system][auth][message]}"] }
        pattern_definitions => {
          "GREEDYMULTILINE"=> "(.|\n)*"
        }
        remove_field => "message"
      }
      date {
        match => [ "[system][auth][timestamp]", "MMM  d HH:mm:ss", "MMM dd HH:mm:ss" ]
      }
      geoip {
        source => "[system][auth][ssh][ip]"
        target => "[system][auth][ssh][geoip]"
      }
    }
    else if [fileset][name] == "syslog" {
      grok {
        match => { "message" => ["%{SYSLOGTIMESTAMP:[system][syslog][timestamp]} %{SYSLOGHOST:[system][syslog][hostname]} %{DATA:[system][syslog][program]}(?:\[%{POSINT:[system][syslog][pid]}\])?: %{GREEDYMULTILINE:[system][syslog][message]}"] }
        pattern_definitions => { "GREEDYMULTILINE" => "(.|\n)*" }
        remove_field => "message"
      }
      date {
        match => [ "[system][syslog][timestamp]", "MMM  d HH:mm:ss", "MMM dd HH:mm:ss" ]
      }
    }
  }
}


output {
  elasticsearch {
    hosts => ["localhost:9200"]
    manage_template => false
    index => "%{[@metadata][beat]}-%{[@metadata][version]}-%{+YYYY.MM.dd}"
  }
}


*********************************************************************************************************************************

#=========================== Filebeat inputs =============================

# List of inputs to fetch data.
filebeat.inputs:
#------------------------------ Log input --------------------------------
- type: log

  # Change to true to enable this input configuration.
  enabled: true

  # Paths that should be crawled and fetched. Glob based paths.
  # To fetch all ".log" files from a specific level of subdirectories /var/log/*/*.log can be used.
  # For each file found under this path, a harvester is started.
  # Make sure no file is defined twice as this can lead to unexpected behaviour.
  paths:
    - "/var/log/nginx/access*.log"
    #- c:\programdata\elasticsearch\logs\*

  # Configure the file encoding for reading files with international characters following the W3C recommendation for HTML5 (http://www.w3.org/TR/encoding).
  # Some sample encodings:
  #   plain, utf-8, utf-16be-bom, utf-16be, utf-16le, big5, gb18030, gbk,
  #    hz-gb-2312, euc-kr, euc-jp, iso-2022-jp, shift-jis, ...
  encoding: plain

  # Include lines. A list of regular expressions to match. It exports the lines that are matching any regular expression from the list.
  # The include_lines is called before exclude_lines. By default, all the lines are exported.
  include_lines: ['^ERR', '^WARN']

  # Exclude lines. A list of regular expressions to match. It drops the lines that are matching any regular expression from the list.
  # The include_lines is called before exclude_lines. By default, no lines are dropped.
  exclude_lines: ['^DBG']

  # Exclude files. A list of regular expressions to match. Filebeat drops the files that are matching any regular expression from the list.
  # By default, no files are dropped.
  exclude_files: ['1.log$']

  # Optional additional fields. These fields can be freely picked to add additional information to the crawled log files for filtering
  # These 4 fields, in particular, are required for Coralogix integration with filebeat to work.
  fields:
    PRIVATE_KEY: "xxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxx"
    COMPANY_ID: XXXX
    APP_NAME: "ngnix"
    SUB_SYSTEM: "ngnix"
    #level: info
    #site: 1

  # Set to true to store the additional fields as top level fields instead
  # of under the "fields" sub-dictionary. In case of name conflicts with the
  # fields added by Filebeat itself, the custom fields overwrite the default
  # fields.
  # In Coralogix we are using it in a different way, if you want to add custom fields then you must set fields under root true. This will also add all metadata from filebeat.
  fields_under_root: true

  ### JSON configuration

  # Decode JSON options. Enable this if your logs are structured in JSON.
  # JSON key on which to apply the line filtering and multiline settings. This key
  # must be top level and its value must be string, otherwise it is ignored. If
  # no text key is defined, the line filtering and multiline features cannot be used.
  json.message_key: "message"

  # By default, the decoded JSON is placed under a "json" key in the output document.
  # If you enable this setting, the keys are copied top level in the output document.
  json.keys_under_root: true

  # If keys_under_root and this setting are enabled, then the values from the decoded
  # JSON object overwrite the fields that Filebeat normally adds (type, source, offset, etc.)
  # in case of conflicts.
  json.overwrite_keys: true

  # If this setting is enabled, Filebeat adds a "error.message" and "error.key: json" key in case of JSON
  # unmarshaling errors or when a text key is defined in the configuration but cannot
  # be used.
  json.add_error_key: false

  ### Multiline options

  # Multiline can be used for log messages spanning multiple lines. This is common
  # for Java Stack Traces or C-Line Continuation

  multiline:
    # The regexp Pattern that has to be matched.
    pattern: '^[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}'
    # Defines if the pattern set under pattern should be negated or not. Default is false.
    negate: true
    # Match can be set to "after" or "before". It is used to define if lines should be append to a pattern
    # that was (not) matched before or after or as long as a pattern is not matched based on negate.
    # Note: After is the equivalent to previous and before is the equivalent to to next in Logstash
    match: after
    # The maximum number of lines that are combined to one event.
    # In case there are more the max_lines the additional lines are discarded.
    # Default is 500
    max_lines: 500
    # After the defined timeout, an multiline event is sent even if no new pattern was found to start a new event
    # Default is 5s.
    timeout: 5s


#- input_type: log
#  paths: /Users/xxxxx/Downloads/elk/logs/app.stderr.log
#  document_type: error
#  multiline.pattern: '^error: '
#  multiline.negate: true
#  multiline.match: after
# ================================= Processors =================================

# Processors are used to reduce the number of fields in the exported event or to
# enhance the event with external metadata. This section defines a list of
# processors that are applied one by one and the first one receives the initial
# event:
#
#   event -> filter1 -> event1 -> filter2 ->event2 ...
#
# The supported processors are drop_fields, drop_event, include_fields,
# decode_json_fields, and add_cloud_metadata.
#
# For example, you can use the following processors to keep the fields that
# contain CPU load percentages, but remove the fields that contain CPU ticks
# values:
#
processors:
  - include_fields:
      fields: ["cpu"]
  - drop_fields:
      fields: ["cpu.user", "cpu.system"]
#
# The following example drops the events that have the HTTP response code 200:
#
processors:
  - drop_event:
      when:
        equals:
          http.code: 200
#
# The following example renames the field a to b:
#
processors:
  - rename:
      fields:
        - from: "a"
          to: "b"
#
# The following example enriches each event with the machine's local time zone
# offset from UTC.
#
processors:
  - add_locale:
      format: offset
#
# The following example enriches each event with host metadata.
#
processors:
  - add_host_metadata: ~
#
# The following example decodes fields containing JSON strings
# and replaces the strings with valid JSON objects.
#
processors:
  - decode_json_fields:
      fields: ["field1", "field2", ...]
      process_array: false
      max_depth: 1
      target: ""
      overwrite_keys: false
#
# The following example copies the value of message to message_copied
#
processors:
  - copy_fields:
      fields:
        - from: message
          to: message_copied
      fail_on_error: true
      ignore_missing: false
#
# The following example preserves the raw message under event_original, which then cutted at 1024 bytes
#
processors:
  - copy_fields:
      fields:
        - from: message
          to: event_original
      fail_on_error: false
      ignore_missing: true
  - truncate_fields:
      fields:
        - event_original
      max_bytes: 1024
      fail_on_error: false
      ignore_missing: true
#
# The following example URL-decodes the value of field1 to field2
#
processors:
  - urldecode:
      fields:
        - from: "field1"
          to: "field2"
      ignore_missing: false
      fail_on_error: true
#
# The following example is a great method to enable sampling in Filebeat, using Script processor
#
processors:
  - script:
      lang: javascript
      id: my_filter
      source: >
        function process(event) {
            if (Math.floor(Math.random() * 100) < 50) {
              event.Cancel();
            }
        }


# ------------------------------ Logstash Output -------------------------------
output.logstash:
  # Boolean flag to enable or disable the output module.
  enabled: true

  # The Logstash hosts
  hosts: ["localhost:5044"]

  # Configure escaping HTML symbols in strings.
  escape_html: true

  # Number of workers per Logstash host.
  worker: 1

  # Optionally load-balance events between Logstash hosts. Default is false.
  loadbalance: false

  # The maximum number of seconds to wait before attempting to connect to
  # Logstash after a network error. The default is 60s.
  backoff.max: 60s

  # Optional index name. The default index name is set to filebeat
  # in all lowercase.
  index: 'filebeat'

  # The number of times to retry publishing an event after a publishing failure.
  # After the specified number of retries, the events are typically dropped.
  # Some Beats, such as Filebeat and Winlogbeat, ignore the max_retries setting
  # and retry until all events are published.  Set max_retries to a value less
  # than 0 to retry until all events are published. The default is 3.
  max_retries: 3

  # The maximum number of events to bulk in a single Logstash request. The
  # default is 2048.
  bulk_max_size: 2048

  # The number of seconds to wait for responses from the Logstash server before
  # timing out. The default is 30s.
  timeout: 30s






ex1:

# ============================== Filebeat Inputs ===============================

filebeat.inputs:
# Use the log input to read lines from log files
- type: log
  # Path of files
  paths:
  - "/var/log/application.log"
  # Include lines. A list of regular expressions to match. It exports the lines that are
  # matching any regular expression from the list. The include_lines is called before
  # exclude_lines. By default, all the lines are exported.
  include_lines: ['^CRITICAL', '^ERROR', '^ERR']
  # Generally, When set to true, the custom fields are stored as top-level fields in the output document instead of being grouped under a fields sub-dictionary.
  # In Coralogix it's a bit different, if you want to add custom fields then you must set fields under root true.
  # This will also add all metadata from fielbeat similar to the native use of this option.
  fields_under_root: true
  # These are the required fields for our integration with filebeat
  fields:
    PRIVATE_KEY: "xxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxx"
    COMPANY_ID: xxxx
    APP_NAME: "prd"
    SUB_SYSTEM: "app"
    # Custom field
    filename: "application.log"

# ================================= Logstash output =================================

output.logstash:
  enabled: true
  # output to Coralogix Logstash server
  hosts: ["logstashserver.coralogix.com:5044"]



  ex2:

  # ============================== Filebeat Inputs ===============================

filebeat.inputs:
# Use the log input to read lines from log files
- type: log
  # Path of files
  paths:
  - "/var/log/filebeat/test.log"
  # These are the required fields for our integration with filebeat
  fields:
    PRIVATE_KEY: "xxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxx"
    COMPANY_ID: xxxx
    APP_NAME: "test"
  # Generally, When set to true, the custom fields are stored as top-level fields in the output document instead of being grouped under a fields sub-dictionary. In Coralogix we are using it in a different way, if you want to add custom fields then you must set fields under root true. This will also add all metadata from fielbeat.
  fields_under_root: true
  # Decode JSON options. Enable this if your logs are structured in JSON.
  json:
    # JSON key on which to apply the line filtering and multiline settings. This key
    # must be top level and its value must be string, otherwise it is ignored. If
    # no text key is defined, the line filtering and multiline features cannot be used.
    message_key: "text"
    # By default, the decoded JSON is placed under a "json" key in the output document.
    # If you enable this setting, the keys are copied top level in the output document.
    keys_under_root: true
    # If keys_under_root and this setting are enabled, then the values from the decoded
    # JSON object overwrite the fields that Filebeat normally adds (type, source, offset, etc.)
    # in case of conflicts.
    overwrite_keys: true  
    # If this setting is enabled, Filebeat adds a "error.message" and "error.key: json" key in case of JSON
    # unmarshaling errors or when a text key is defined in the configuration but cannot
    # be used.
    add_error_key: false

# ================================= Processors =================================

processors:
  # This processor will extract the value from a JSON key into Coralogix subsystem name
  - copy_fields:
      fields:
        - from: company
          to: SUB_SYSTEM
      fail_on_error: false
      ignore_missing: true

# ================================= Logstash output =================================

output.logstash:
  enabled: true
  # output to Coralogix Logstash server
  hosts: ["logstashserver.coralogix.com:5044"]



  ex3:

# ============================== Filebeat Inputs ===============================

filebeat.inputs:
# Use the log input to read lines from log files
- type: log
  # Path of files
  paths:
  - "/var/log/filebeat/access.log"
  ignore_older: 1h
  # Adding custom fields
  fields:
    user: "coralogix"
  # When set to true, the custom fields are stored as top-level fields in the output document instead of being grouped under a fields sub-dictionary.
  fields_under_root: true

  # Set multiline pattern - This form of multiline will append Consecutive lines that don’t match the pattern to the previous line that does match
  multiline:
    pattern: '^[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}'
    negate: true
    match: after

# ================================= Processors =================================

processors:
  - drop_fields:
      fields: ["input", "beat_host", "ecs", "agent", "tags", "offset"]
      ignore_missing: true

# ================================= Console output =================================

output.console:
  pretty: true


ex4:

# ============================== Filebeat Inputs ===============================

filebeat.inputs:
- type: redis
  # List of hosts to pool to retrieve the slow log information.
  hosts: ["localhost:6379"]
  # How often the input checks for redis slow log.
  scan_frequency: 10s
  # Redis AUTH password. Empty by default.
  password: "${redis_pwd}"
  # These are the required fields for our integration with filebeat
  fields:
    PRIVATE_KEY: "xxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxx"
    COMPANY_ID: xxxx
    APP_NAME: "filebeat"
    SUB_SYSTEM: "redis"
  # Generally, When set to true, the custom fields are stored as top-level fields in the output document instead of being grouped under a fields sub-dictionary. In Coralogix we are using it in a different way, if you want to add custom fields then you must set fields under root true. This will also add all metadata from fielbeat.
  fields_under_root: true

# ================================= Logstash output =================================

output.logstash:
  enabled: true
  # output to Coralogix Logstash server
  # If you want to use an encrypted connection, you need to add our certificates as described in our filebeat tutorial
  hosts: ["logstashserver.coralogix.com:5015"]
  tls.certificate_authorities: ["<path to folder with certificates>/ca.crt"]
  ssl.certificate_authorities: ["<path to folder with certificates>/ca.crt"]






Logstash - Email plugging,

input { http { } }

output {
  email {
    to => '%{destination_mail}'
    address => 'mail server hostname'
    subject => 'Alert - %{trigger}'
    body => "Trigger: %{trigger}\n\nMonitor: %{monitor}\n\nSeverity: %{severity}\n\nDescription: %{description}"
    port => 25
  }
}


filter {
    if [trigger] =~ ".*SYSOPS.*" {
      mutate {
          add_field => { "destination_mail" => "sysops@mail.com" }
      }
    }
    else if [trigger] =~ ".*ARCHITECTURE.*" {
      mutate {
          add_field => { "destination_mail" => "architecture@mail.com" }
      }
    }
    
    https://medium.com/bluekiri/kibana-mail-alerting-with-logstash-43fb9e4789f2

Change format for fields

filter {
  mutate {
    convert => { "fieldname" => "integer" }
  }
}


...



ElasticSearch'de Su Hatayı alıyorsan Ilacin bende🌼 "TOO_MANY_REQUESTS/12/disk usage exceeded flood-stage watermark...."

Sebep: Dun gece zabaaha kadar bu sorunlarla ugrastim :) Daha 10larca farklı hatala da ugrastim🌼 Kibana'nin Indexlerini, Login olamamasindan dolay, Elastic'den sildikten sonra bu hata ile karsilastim :) 

COZUM:
1-) curl -XPUT -H "Content-Type: application/json" http://serverIP:9200/_all/_settings -d '{"index.blocks.read_only_allow_delete": null}' -u USERNAME:PASSWORD

2-) curl -X PUT "http://serverIP:9200/_cluster/settings?pretty" -H 'Content-Type: application/json' -d' { "transient": { "cluster.routing.allocation.disk.watermark.low": "50gb", "cluster.routing.allocation.disk.watermark.high": "20gb", "cluster.routing.allocation.disk.watermark.flood_stage": "10gb", "cluster.info.update.interval": "1m"}}' -u USERNAME:PASSWORD

1-)(TEKRAR) (Tekrar Readonly olmasın diye calistir.) curl -XPUT -H "Content-Type: application/json" http://serverIP:9200/_all/_settings -d '{"index.blocks.read_only_allow_delete": null}' -u USERNAME:PASSWORD
ADVANCE TUNING -- > https://www.elastic.co/blog/advanced-tuning-finding-and-fixing-slow-elasticsearch-queries
kibana overview -- > https://logit.io/blog/post/the-top-kibana-dashboards-and-visualisations



```

```
VALIDATE CONFIGURATION
sudo metricbeat -e -c /etc/metricbeat/metricbeat.yml
metricbeat
metricbeat test config

sudo systemctl enable metricbeat
sudo systemctl start metricbeat
```
`  ignore_older: 1h ### IMPORTANT ###`

if time to late; you must change config
if \"hostx03\" not in [agent.hostname] or \"hostx04\" not in [agent.hostname]  {
  or 
if logfilepath=="space" .. else {drop}


[Elastic-Alert](https://www.youtube.com/watch?v=vhRxUrg2OxM&list=PL34sAs7_26wOgpqMW_0_E95k9tq2VkMOZ&index=9)

[blog-other](https://qbox.io/blog/indexing-emails-to-elasticsearch-logstash-imap/)

[kube monitoring with elk stack deployment](https://www.youtube.com/watch?v=b4wOV6vlqPU&t=54s)

[alpha2](https://xeraa.net/blog/2021_elastic-stack-8-0-0-alpha2/)


[Elasticsearch Index Speed Optimization](https://medium.com/trendyol-tech/elasticsearch-index-speed-optimization-ec37684041dc)


[lab-kubernetes](https://www.katacoda.com/dan_roscigno/scenarios/logs-and-metrics-elasticsearch-kibana)


[what is elasticsearch](https://www.elastic.co/what-is/elasticsearch)

[elasticsearch future](https://www.elastic.co/elasticsearch/features)

[cloud elastic](https://www.elastic.co/cloud/elasticsearch-service/signup)

[haydarKulekciElasticBlog](https://elasticsearch.kulekci.net/)

[AsiyeYigitElasticXpackSecurity](https://asiyeyigit.com/elastic-security/)


[MultipleGrokStatement](https://dzone.com/articles/using-multiple-grok-statements)


[elasticVue Extention](https://chrome.google.com/webstore/detail/elasticvue/hkedbapjpblbodpgbajblpnlpenaebaa)

[fluent](https://s-fazil-yesil.medium.com/uygulama-loglar%C4%B1n%C4%B1n-fluentd-ile-toplan%C4%B1p-merkezi-log-sistemine-aktar%C4%B1m%C4%B1-3806329aa582)
