*/10 * * * * 

#!/bin/bash

#Filebeat status check

ps -ef |grep filebeat

status=$(systemctl status filebeat | awk 'NR==3{print $2}')

echo $status
if [[ $status == active ]]
        then
            echo "OK 1"

else
         systemctl start filebeat

fi

Echo "filebeat started"
